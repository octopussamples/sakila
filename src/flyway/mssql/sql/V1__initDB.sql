
 IF NOT EXISTS(SELECT * FROM sys.schemas WHERE [name] = N'db_accessadmin')      
     EXEC (N'CREATE SCHEMA db_accessadmin')                                   
 GO                                                               
 IF NOT EXISTS(SELECT * FROM sys.schemas WHERE [name] = N'db_backupoperator')      
     EXEC (N'CREATE SCHEMA db_backupoperator')                                   
 GO                                                               

 IF NOT EXISTS(SELECT * FROM sys.schemas WHERE [name] = N'db_datareader')      
     EXEC (N'CREATE SCHEMA db_datareader')                                   
 GO                                                               

 IF NOT EXISTS(SELECT * FROM sys.schemas WHERE [name] = N'db_datawriter')      
     EXEC (N'CREATE SCHEMA db_datawriter')                                   
 GO                                                               

 IF NOT EXISTS(SELECT * FROM sys.schemas WHERE [name] = N'db_ddladmin')      
     EXEC (N'CREATE SCHEMA db_ddladmin')                                   
 GO                                                               

 IF NOT EXISTS(SELECT * FROM sys.schemas WHERE [name] = N'db_denydatareader')      
     EXEC (N'CREATE SCHEMA db_denydatareader')                                   
 GO                                                               

 IF NOT EXISTS(SELECT * FROM sys.schemas WHERE [name] = N'db_denydatawriter')      
     EXEC (N'CREATE SCHEMA db_denydatawriter')                                   
 GO                                                               

 IF NOT EXISTS(SELECT * FROM sys.schemas WHERE [name] = N'db_owner')      
     EXEC (N'CREATE SCHEMA db_owner')                                   
 GO                                                               

 IF NOT EXISTS(SELECT * FROM sys.schemas WHERE [name] = N'db_securityadmin')      
     EXEC (N'CREATE SCHEMA db_securityadmin')                                   
 GO                                                               

 IF NOT EXISTS(SELECT * FROM sys.schemas WHERE [name] = N'dbo')      
     EXEC (N'CREATE SCHEMA dbo')                                   
 GO                                                               

 IF NOT EXISTS(SELECT * FROM sys.schemas WHERE [name] = N'guest')      
     EXEC (N'CREATE SCHEMA guest')                                   
 GO                                                               

 IF NOT EXISTS(SELECT * FROM sys.schemas WHERE [name] = N'INFORMATION_SCHEMA')      
     EXEC (N'CREATE SCHEMA INFORMATION_SCHEMA')                                   
 GO                                                               

 IF NOT EXISTS(SELECT * FROM sys.schemas WHERE [name] = N'dbo')      
     EXEC (N'CREATE SCHEMA dbo')                                   
 GO                                                               

 IF NOT EXISTS(SELECT * FROM sys.schemas WHERE [name] = N'sys')      
     EXEC (N'CREATE SCHEMA sys')                                   
 GO                                                               

IF EXISTS (SELECT * FROM sys.objects so JOIN sys.schemas sc ON so.schema_id = sc.schema_id WHERE so.name = N'actor'  AND sc.name = N'dbo'  AND type in (N'U'))
BEGIN

  DECLARE @drop_statement nvarchar(500)

  DECLARE drop_cursor CURSOR FOR
      SELECT 'alter table '+quotename(schema_name(ob.schema_id))+
      '.'+quotename(object_name(ob.object_id))+ ' drop constraint ' + quotename(fk.name) 
      FROM sys.objects ob INNER JOIN sys.foreign_keys fk ON fk.parent_object_id = ob.object_id
      WHERE fk.referenced_object_id = 
          (
             SELECT so.object_id 
             FROM sys.objects so JOIN sys.schemas sc
             ON so.schema_id = sc.schema_id
             WHERE so.name = N'actor'  AND sc.name = N'dbo'  AND type in (N'U')
           )

  OPEN drop_cursor

  FETCH NEXT FROM drop_cursor
  INTO @drop_statement

  WHILE @@FETCH_STATUS = 0
  BEGIN
     EXEC (@drop_statement)

     FETCH NEXT FROM drop_cursor
     INTO @drop_statement
  END

  CLOSE drop_cursor
  DEALLOCATE drop_cursor

  DROP TABLE [actor]
END 
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE 
[actor]
(
   [actor_id] int IDENTITY(1, 1)  NOT NULL,
   [first_name] nvarchar(45)  NOT NULL,
   [last_name] nvarchar(45)  NOT NULL,
   [last_update] datetime  NOT NULL
)
WITH (DATA_COMPRESSION = NONE)
GO


GO
IF EXISTS (SELECT * FROM sys.objects so JOIN sys.schemas sc ON so.schema_id = sc.schema_id WHERE so.name = N'address'  AND sc.name = N'dbo'  AND type in (N'U'))
BEGIN

  DECLARE @drop_statement nvarchar(500)

  DECLARE drop_cursor CURSOR FOR
      SELECT 'alter table '+quotename(schema_name(ob.schema_id))+
      '.'+quotename(object_name(ob.object_id))+ ' drop constraint ' + quotename(fk.name) 
      FROM sys.objects ob INNER JOIN sys.foreign_keys fk ON fk.parent_object_id = ob.object_id
      WHERE fk.referenced_object_id = 
          (
             SELECT so.object_id 
             FROM sys.objects so JOIN sys.schemas sc
             ON so.schema_id = sc.schema_id
             WHERE so.name = N'address'  AND sc.name = N'dbo'  AND type in (N'U')
           )

  OPEN drop_cursor

  FETCH NEXT FROM drop_cursor
  INTO @drop_statement

  WHILE @@FETCH_STATUS = 0
  BEGIN
     EXEC (@drop_statement)

     FETCH NEXT FROM drop_cursor
     INTO @drop_statement
  END

  CLOSE drop_cursor
  DEALLOCATE drop_cursor

  DROP TABLE [address]
END 
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE 
[address]
(
   [address_id] int IDENTITY(1, 1)  NOT NULL,
   [address] nvarchar(50)  NOT NULL,
   [address2] nvarchar(50)  NULL,
   [district] nvarchar(20)  NOT NULL,
   [city_id] int  NOT NULL,
   [postal_code] nvarchar(10)  NULL,
   [phone] nvarchar(20)  NOT NULL,
   [location] geometry  NOT NULL,
   [last_update] datetime  NOT NULL
)
WITH (DATA_COMPRESSION = NONE)
GO


GO
IF EXISTS (SELECT * FROM sys.objects so JOIN sys.schemas sc ON so.schema_id = sc.schema_id WHERE so.name = N'category'  AND sc.name = N'dbo'  AND type in (N'U'))
BEGIN

  DECLARE @drop_statement nvarchar(500)

  DECLARE drop_cursor CURSOR FOR
      SELECT 'alter table '+quotename(schema_name(ob.schema_id))+
      '.'+quotename(object_name(ob.object_id))+ ' drop constraint ' + quotename(fk.name) 
      FROM sys.objects ob INNER JOIN sys.foreign_keys fk ON fk.parent_object_id = ob.object_id
      WHERE fk.referenced_object_id = 
          (
             SELECT so.object_id 
             FROM sys.objects so JOIN sys.schemas sc
             ON so.schema_id = sc.schema_id
             WHERE so.name = N'category'  AND sc.name = N'dbo'  AND type in (N'U')
           )

  OPEN drop_cursor

  FETCH NEXT FROM drop_cursor
  INTO @drop_statement

  WHILE @@FETCH_STATUS = 0
  BEGIN
     EXEC (@drop_statement)

     FETCH NEXT FROM drop_cursor
     INTO @drop_statement
  END

  CLOSE drop_cursor
  DEALLOCATE drop_cursor

  DROP TABLE [category]
END 
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE 
[category]
(
   [category_id] tinyint IDENTITY(1, 1)  NOT NULL,
   [name] nvarchar(25)  NOT NULL,
   [last_update] datetime  NOT NULL
)
WITH (DATA_COMPRESSION = NONE)
GO



GO
IF EXISTS (SELECT * FROM sys.objects so JOIN sys.schemas sc ON so.schema_id = sc.schema_id WHERE so.name = N'city'  AND sc.name = N'dbo'  AND type in (N'U'))
BEGIN

  DECLARE @drop_statement nvarchar(500)

  DECLARE drop_cursor CURSOR FOR
      SELECT 'alter table '+quotename(schema_name(ob.schema_id))+
      '.'+quotename(object_name(ob.object_id))+ ' drop constraint ' + quotename(fk.name) 
      FROM sys.objects ob INNER JOIN sys.foreign_keys fk ON fk.parent_object_id = ob.object_id
      WHERE fk.referenced_object_id = 
          (
             SELECT so.object_id 
             FROM sys.objects so JOIN sys.schemas sc
             ON so.schema_id = sc.schema_id
             WHERE so.name = N'city'  AND sc.name = N'dbo'  AND type in (N'U')
           )

  OPEN drop_cursor

  FETCH NEXT FROM drop_cursor
  INTO @drop_statement

  WHILE @@FETCH_STATUS = 0
  BEGIN
     EXEC (@drop_statement)

     FETCH NEXT FROM drop_cursor
     INTO @drop_statement
  END

  CLOSE drop_cursor
  DEALLOCATE drop_cursor

  DROP TABLE [city]
END 
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE 
[city]
(
   [city_id] int IDENTITY(1, 1)  NOT NULL,
   [city] nvarchar(50)  NOT NULL,
   [country_id] int  NOT NULL,
   [last_update] datetime  NOT NULL
)
WITH (DATA_COMPRESSION = NONE)
GO


GO
IF EXISTS (SELECT * FROM sys.objects so JOIN sys.schemas sc ON so.schema_id = sc.schema_id WHERE so.name = N'country'  AND sc.name = N'dbo'  AND type in (N'U'))
BEGIN

  DECLARE @drop_statement nvarchar(500)

  DECLARE drop_cursor CURSOR FOR
      SELECT 'alter table '+quotename(schema_name(ob.schema_id))+
      '.'+quotename(object_name(ob.object_id))+ ' drop constraint ' + quotename(fk.name) 
      FROM sys.objects ob INNER JOIN sys.foreign_keys fk ON fk.parent_object_id = ob.object_id
      WHERE fk.referenced_object_id = 
          (
             SELECT so.object_id 
             FROM sys.objects so JOIN sys.schemas sc
             ON so.schema_id = sc.schema_id
             WHERE so.name = N'country'  AND sc.name = N'dbo'  AND type in (N'U')
           )

  OPEN drop_cursor

  FETCH NEXT FROM drop_cursor
  INTO @drop_statement

  WHILE @@FETCH_STATUS = 0
  BEGIN
     EXEC (@drop_statement)

     FETCH NEXT FROM drop_cursor
     INTO @drop_statement
  END

  CLOSE drop_cursor
  DEALLOCATE drop_cursor

  DROP TABLE [country]
END 
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE 
[country]
(
   [country_id] int IDENTITY(1, 1)  NOT NULL,
   [country] nvarchar(50)  NOT NULL,
   [last_update] datetime  NOT NULL
)
WITH (DATA_COMPRESSION = NONE)
GO


GO
IF EXISTS (SELECT * FROM sys.objects so JOIN sys.schemas sc ON so.schema_id = sc.schema_id WHERE so.name = N'customer'  AND sc.name = N'dbo'  AND type in (N'U'))
BEGIN

  DECLARE @drop_statement nvarchar(500)

  DECLARE drop_cursor CURSOR FOR
      SELECT 'alter table '+quotename(schema_name(ob.schema_id))+
      '.'+quotename(object_name(ob.object_id))+ ' drop constraint ' + quotename(fk.name) 
      FROM sys.objects ob INNER JOIN sys.foreign_keys fk ON fk.parent_object_id = ob.object_id
      WHERE fk.referenced_object_id = 
          (
             SELECT so.object_id 
             FROM sys.objects so JOIN sys.schemas sc
             ON so.schema_id = sc.schema_id
             WHERE so.name = N'customer'  AND sc.name = N'dbo'  AND type in (N'U')
           )

  OPEN drop_cursor

  FETCH NEXT FROM drop_cursor
  INTO @drop_statement

  WHILE @@FETCH_STATUS = 0
  BEGIN
     EXEC (@drop_statement)

     FETCH NEXT FROM drop_cursor
     INTO @drop_statement
  END

  CLOSE drop_cursor
  DEALLOCATE drop_cursor

  DROP TABLE [customer]
END 
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE 
[customer]
(
   [customer_id] int IDENTITY(1, 1)  NOT NULL,
   [store_id] tinyint  NOT NULL,
   [first_name] nvarchar(45)  NOT NULL,
   [last_name] nvarchar(45)  NOT NULL,
   [email] nvarchar(50)  NULL,
   [address_id] int  NOT NULL,

   /*
   *   SSMA informational messages:
   *   M2SS0052: BIT literal was converted to BINARY literal
   */

   [active] binary(1)  NOT NULL,
   [create_date] datetime2(0)  NOT NULL,
   [last_update] datetime  NULL
)
WITH (DATA_COMPRESSION = NONE)
GO


GO
IF EXISTS (SELECT * FROM sys.objects so JOIN sys.schemas sc ON so.schema_id = sc.schema_id WHERE so.name = N'databasechangelog'  AND sc.name = N'dbo'  AND type in (N'U'))
BEGIN

  DECLARE @drop_statement nvarchar(500)

  DECLARE drop_cursor CURSOR FOR
      SELECT 'alter table '+quotename(schema_name(ob.schema_id))+
      '.'+quotename(object_name(ob.object_id))+ ' drop constraint ' + quotename(fk.name) 
      FROM sys.objects ob INNER JOIN sys.foreign_keys fk ON fk.parent_object_id = ob.object_id
      WHERE fk.referenced_object_id = 
          (
             SELECT so.object_id 
             FROM sys.objects so JOIN sys.schemas sc
             ON so.schema_id = sc.schema_id
             WHERE so.name = N'databasechangelog'  AND sc.name = N'dbo'  AND type in (N'U')
           )

  OPEN drop_cursor

  FETCH NEXT FROM drop_cursor
  INTO @drop_statement

  WHILE @@FETCH_STATUS = 0
  BEGIN
     EXEC (@drop_statement)

     FETCH NEXT FROM drop_cursor
     INTO @drop_statement
  END

  CLOSE drop_cursor
  DEALLOCATE drop_cursor

  DROP TABLE [databasechangelog]
END 
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE 
[databasechangelog]
(
   [ID] nvarchar(255)  NOT NULL,
   [AUTHOR] nvarchar(255)  NOT NULL,
   [FILENAME] nvarchar(255)  NOT NULL,
   [DATEEXECUTED] datetime2(0)  NOT NULL,
   [ORDEREXECUTED] int  NOT NULL,
   [EXECTYPE] nvarchar(10)  NOT NULL,
   [MD5SUM] nvarchar(35)  NULL,
   [DESCRIPTION] nvarchar(255)  NULL,
   [COMMENTS] nvarchar(255)  NULL,
   [TAG] nvarchar(255)  NULL,
   [LIQUIBASE] nvarchar(20)  NULL,
   [CONTEXTS] nvarchar(255)  NULL,
   [LABELS] nvarchar(255)  NULL,
   [DEPLOYMENT_ID] nvarchar(10)  NULL
)
WITH (DATA_COMPRESSION = NONE)
GO


GO
IF EXISTS (SELECT * FROM sys.objects so JOIN sys.schemas sc ON so.schema_id = sc.schema_id WHERE so.name = N'databasechangeloglock'  AND sc.name = N'dbo'  AND type in (N'U'))
BEGIN

  DECLARE @drop_statement nvarchar(500)

  DECLARE drop_cursor CURSOR FOR
      SELECT 'alter table '+quotename(schema_name(ob.schema_id))+
      '.'+quotename(object_name(ob.object_id))+ ' drop constraint ' + quotename(fk.name) 
      FROM sys.objects ob INNER JOIN sys.foreign_keys fk ON fk.parent_object_id = ob.object_id
      WHERE fk.referenced_object_id = 
          (
             SELECT so.object_id 
             FROM sys.objects so JOIN sys.schemas sc
             ON so.schema_id = sc.schema_id
             WHERE so.name = N'databasechangeloglock'  AND sc.name = N'dbo'  AND type in (N'U')
           )

  OPEN drop_cursor

  FETCH NEXT FROM drop_cursor
  INTO @drop_statement

  WHILE @@FETCH_STATUS = 0
  BEGIN
     EXEC (@drop_statement)

     FETCH NEXT FROM drop_cursor
     INTO @drop_statement
  END

  CLOSE drop_cursor
  DEALLOCATE drop_cursor

  DROP TABLE [databasechangeloglock]
END 
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE 
[databasechangeloglock]
(
   [ID] int  NOT NULL,
   [LOCKED] binary(1)  NOT NULL,
   [LOCKGRANTED] datetime2(0)  NULL,
   [LOCKEDBY] nvarchar(255)  NULL
)
WITH (DATA_COMPRESSION = NONE)
GO


GO
IF EXISTS (SELECT * FROM sys.objects so JOIN sys.schemas sc ON so.schema_id = sc.schema_id WHERE so.name = N'film'  AND sc.name = N'dbo'  AND type in (N'U'))
BEGIN

  DECLARE @drop_statement nvarchar(500)

  DECLARE drop_cursor CURSOR FOR
      SELECT 'alter table '+quotename(schema_name(ob.schema_id))+
      '.'+quotename(object_name(ob.object_id))+ ' drop constraint ' + quotename(fk.name) 
      FROM sys.objects ob INNER JOIN sys.foreign_keys fk ON fk.parent_object_id = ob.object_id
      WHERE fk.referenced_object_id = 
          (
             SELECT so.object_id 
             FROM sys.objects so JOIN sys.schemas sc
             ON so.schema_id = sc.schema_id
             WHERE so.name = N'film'  AND sc.name = N'dbo'  AND type in (N'U')
           )

  OPEN drop_cursor

  FETCH NEXT FROM drop_cursor
  INTO @drop_statement

  WHILE @@FETCH_STATUS = 0
  BEGIN
     EXEC (@drop_statement)

     FETCH NEXT FROM drop_cursor
     INTO @drop_statement
  END

  CLOSE drop_cursor
  DEALLOCATE drop_cursor

  DROP TABLE [film]
END 
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE 
[film]
(
   [film_id] int IDENTITY(1, 1)  NOT NULL,
   [title] nvarchar(128)  NOT NULL,
   [description] nvarchar(max)  NULL,
   [release_year] smallint  NULL,
   [language_id] tinyint  NOT NULL,
   [original_language_id] tinyint  NULL,

   /*
   *   SSMA informational messages:
   *   M2SS0052: string literal was converted to NUMERIC literal
   */

   [rental_duration] tinyint  NOT NULL,

   /*
   *   SSMA informational messages:
   *   M2SS0052: string literal was converted to NUMERIC literal
   */

   [rental_rate] decimal(4, 2)  NOT NULL,
   [length] int  NULL,

   /*
   *   SSMA informational messages:
   *   M2SS0052: string literal was converted to NUMERIC literal
   */

   [replacement_cost] decimal(5, 2)  NOT NULL,
   [rating] nvarchar(5)  NULL,
   [special_features] nvarchar(54)  NULL,
   [last_update] datetime  NOT NULL
)
WITH (DATA_COMPRESSION = NONE)
GO


GO
IF EXISTS (SELECT * FROM sys.objects so JOIN sys.schemas sc ON so.schema_id = sc.schema_id WHERE so.name = N'film_actor'  AND sc.name = N'dbo'  AND type in (N'U'))
BEGIN

  DECLARE @drop_statement nvarchar(500)

  DECLARE drop_cursor CURSOR FOR
      SELECT 'alter table '+quotename(schema_name(ob.schema_id))+
      '.'+quotename(object_name(ob.object_id))+ ' drop constraint ' + quotename(fk.name) 
      FROM sys.objects ob INNER JOIN sys.foreign_keys fk ON fk.parent_object_id = ob.object_id
      WHERE fk.referenced_object_id = 
          (
             SELECT so.object_id 
             FROM sys.objects so JOIN sys.schemas sc
             ON so.schema_id = sc.schema_id
             WHERE so.name = N'film_actor'  AND sc.name = N'dbo'  AND type in (N'U')
           )

  OPEN drop_cursor

  FETCH NEXT FROM drop_cursor
  INTO @drop_statement

  WHILE @@FETCH_STATUS = 0
  BEGIN
     EXEC (@drop_statement)

     FETCH NEXT FROM drop_cursor
     INTO @drop_statement
  END

  CLOSE drop_cursor
  DEALLOCATE drop_cursor

  DROP TABLE [film_actor]
END 
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE 
[film_actor]
(
   [actor_id] int  NOT NULL,
   [film_id] int  NOT NULL,
   [last_update] datetime  NOT NULL
)
WITH (DATA_COMPRESSION = NONE)
GO


GO
IF EXISTS (SELECT * FROM sys.objects so JOIN sys.schemas sc ON so.schema_id = sc.schema_id WHERE so.name = N'film_category'  AND sc.name = N'dbo'  AND type in (N'U'))
BEGIN

  DECLARE @drop_statement nvarchar(500)

  DECLARE drop_cursor CURSOR FOR
      SELECT 'alter table '+quotename(schema_name(ob.schema_id))+
      '.'+quotename(object_name(ob.object_id))+ ' drop constraint ' + quotename(fk.name) 
      FROM sys.objects ob INNER JOIN sys.foreign_keys fk ON fk.parent_object_id = ob.object_id
      WHERE fk.referenced_object_id = 
          (
             SELECT so.object_id 
             FROM sys.objects so JOIN sys.schemas sc
             ON so.schema_id = sc.schema_id
             WHERE so.name = N'film_category'  AND sc.name = N'dbo'  AND type in (N'U')
           )

  OPEN drop_cursor

  FETCH NEXT FROM drop_cursor
  INTO @drop_statement

  WHILE @@FETCH_STATUS = 0
  BEGIN
     EXEC (@drop_statement)

     FETCH NEXT FROM drop_cursor
     INTO @drop_statement
  END

  CLOSE drop_cursor
  DEALLOCATE drop_cursor

  DROP TABLE [film_category]
END 
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE 
[film_category]
(
   [film_id] int  NOT NULL,
   [category_id] tinyint  NOT NULL,
   [last_update] datetime  NOT NULL
)
WITH (DATA_COMPRESSION = NONE)
GO


GO
IF EXISTS (SELECT * FROM sys.objects so JOIN sys.schemas sc ON so.schema_id = sc.schema_id WHERE so.name = N'film_text'  AND sc.name = N'dbo'  AND type in (N'U'))
BEGIN

  DECLARE @drop_statement nvarchar(500)

  DECLARE drop_cursor CURSOR FOR
      SELECT 'alter table '+quotename(schema_name(ob.schema_id))+
      '.'+quotename(object_name(ob.object_id))+ ' drop constraint ' + quotename(fk.name) 
      FROM sys.objects ob INNER JOIN sys.foreign_keys fk ON fk.parent_object_id = ob.object_id
      WHERE fk.referenced_object_id = 
          (
             SELECT so.object_id 
             FROM sys.objects so JOIN sys.schemas sc
             ON so.schema_id = sc.schema_id
             WHERE so.name = N'film_text'  AND sc.name = N'dbo'  AND type in (N'U')
           )

  OPEN drop_cursor

  FETCH NEXT FROM drop_cursor
  INTO @drop_statement

  WHILE @@FETCH_STATUS = 0
  BEGIN
     EXEC (@drop_statement)

     FETCH NEXT FROM drop_cursor
     INTO @drop_statement
  END

  CLOSE drop_cursor
  DEALLOCATE drop_cursor

  DROP TABLE [film_text]
END 
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE 
[film_text]
(
   [film_id] smallint  NOT NULL,
   [title] nvarchar(255)  NOT NULL,
   [description] nvarchar(max)  NULL
)
WITH (DATA_COMPRESSION = NONE)
GO


GO
IF EXISTS (SELECT * FROM sys.objects so JOIN sys.schemas sc ON so.schema_id = sc.schema_id WHERE so.name = N'inventory'  AND sc.name = N'dbo'  AND type in (N'U'))
BEGIN

  DECLARE @drop_statement nvarchar(500)

  DECLARE drop_cursor CURSOR FOR
      SELECT 'alter table '+quotename(schema_name(ob.schema_id))+
      '.'+quotename(object_name(ob.object_id))+ ' drop constraint ' + quotename(fk.name) 
      FROM sys.objects ob INNER JOIN sys.foreign_keys fk ON fk.parent_object_id = ob.object_id
      WHERE fk.referenced_object_id = 
          (
             SELECT so.object_id 
             FROM sys.objects so JOIN sys.schemas sc
             ON so.schema_id = sc.schema_id
             WHERE so.name = N'inventory'  AND sc.name = N'dbo'  AND type in (N'U')
           )

  OPEN drop_cursor

  FETCH NEXT FROM drop_cursor
  INTO @drop_statement

  WHILE @@FETCH_STATUS = 0
  BEGIN
     EXEC (@drop_statement)

     FETCH NEXT FROM drop_cursor
     INTO @drop_statement
  END

  CLOSE drop_cursor
  DEALLOCATE drop_cursor

  DROP TABLE [inventory]
END 
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE 
[inventory]
(
   [inventory_id] int IDENTITY(1, 1)  NOT NULL,
   [film_id] int  NOT NULL,
   [store_id] tinyint  NOT NULL,
   [last_update] datetime  NOT NULL
)
WITH (DATA_COMPRESSION = NONE)
GO


GO
IF EXISTS (SELECT * FROM sys.objects so JOIN sys.schemas sc ON so.schema_id = sc.schema_id WHERE so.name = N'language'  AND sc.name = N'dbo'  AND type in (N'U'))
BEGIN

  DECLARE @drop_statement nvarchar(500)

  DECLARE drop_cursor CURSOR FOR
      SELECT 'alter table '+quotename(schema_name(ob.schema_id))+
      '.'+quotename(object_name(ob.object_id))+ ' drop constraint ' + quotename(fk.name) 
      FROM sys.objects ob INNER JOIN sys.foreign_keys fk ON fk.parent_object_id = ob.object_id
      WHERE fk.referenced_object_id = 
          (
             SELECT so.object_id 
             FROM sys.objects so JOIN sys.schemas sc
             ON so.schema_id = sc.schema_id
             WHERE so.name = N'language'  AND sc.name = N'dbo'  AND type in (N'U')
           )

  OPEN drop_cursor

  FETCH NEXT FROM drop_cursor
  INTO @drop_statement

  WHILE @@FETCH_STATUS = 0
  BEGIN
     EXEC (@drop_statement)

     FETCH NEXT FROM drop_cursor
     INTO @drop_statement
  END

  CLOSE drop_cursor
  DEALLOCATE drop_cursor

  DROP TABLE [language]
END 
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE 
[language]
(
   [language_id] tinyint IDENTITY(1, 1)  NOT NULL,
   [name] nchar(20)  NOT NULL,
   [last_update] datetime  NOT NULL
)
WITH (DATA_COMPRESSION = NONE)
GO


GO
IF EXISTS (SELECT * FROM sys.objects so JOIN sys.schemas sc ON so.schema_id = sc.schema_id WHERE so.name = N'payment'  AND sc.name = N'dbo'  AND type in (N'U'))
BEGIN

  DECLARE @drop_statement nvarchar(500)

  DECLARE drop_cursor CURSOR FOR
      SELECT 'alter table '+quotename(schema_name(ob.schema_id))+
      '.'+quotename(object_name(ob.object_id))+ ' drop constraint ' + quotename(fk.name) 
      FROM sys.objects ob INNER JOIN sys.foreign_keys fk ON fk.parent_object_id = ob.object_id
      WHERE fk.referenced_object_id = 
          (
             SELECT so.object_id 
             FROM sys.objects so JOIN sys.schemas sc
             ON so.schema_id = sc.schema_id
             WHERE so.name = N'payment'  AND sc.name = N'dbo'  AND type in (N'U')
           )

  OPEN drop_cursor

  FETCH NEXT FROM drop_cursor
  INTO @drop_statement

  WHILE @@FETCH_STATUS = 0
  BEGIN
     EXEC (@drop_statement)

     FETCH NEXT FROM drop_cursor
     INTO @drop_statement
  END

  CLOSE drop_cursor
  DEALLOCATE drop_cursor

  DROP TABLE [payment]
END 
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE 
[payment]
(
   [payment_id] int IDENTITY(1, 1)  NOT NULL,
   [customer_id] int  NOT NULL,
   [staff_id] tinyint  NOT NULL,
   [rental_id] int  NULL,
   [amount] decimal(5, 2)  NOT NULL,
   [payment_date] datetime2(0)  NOT NULL,
   [last_update] datetime  NULL
)
WITH (DATA_COMPRESSION = NONE)
GO


GO
IF EXISTS (SELECT * FROM sys.objects so JOIN sys.schemas sc ON so.schema_id = sc.schema_id WHERE so.name = N'rental'  AND sc.name = N'dbo'  AND type in (N'U'))
BEGIN

  DECLARE @drop_statement nvarchar(500)

  DECLARE drop_cursor CURSOR FOR
      SELECT 'alter table '+quotename(schema_name(ob.schema_id))+
      '.'+quotename(object_name(ob.object_id))+ ' drop constraint ' + quotename(fk.name) 
      FROM sys.objects ob INNER JOIN sys.foreign_keys fk ON fk.parent_object_id = ob.object_id
      WHERE fk.referenced_object_id = 
          (
             SELECT so.object_id 
             FROM sys.objects so JOIN sys.schemas sc
             ON so.schema_id = sc.schema_id
             WHERE so.name = N'rental'  AND sc.name = N'dbo'  AND type in (N'U')
           )

  OPEN drop_cursor

  FETCH NEXT FROM drop_cursor
  INTO @drop_statement

  WHILE @@FETCH_STATUS = 0
  BEGIN
     EXEC (@drop_statement)

     FETCH NEXT FROM drop_cursor
     INTO @drop_statement
  END

  CLOSE drop_cursor
  DEALLOCATE drop_cursor

  DROP TABLE [rental]
END 
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE 
[rental]
(
   [rental_id] int IDENTITY(1, 1)  NOT NULL,
   [rental_date] datetime2(0)  NOT NULL,
   [inventory_id] int  NOT NULL,
   [customer_id] int  NOT NULL,
   [return_date] datetime2(0)  NULL,
   [staff_id] tinyint  NOT NULL,
   [last_update] datetime  NOT NULL
)
WITH (DATA_COMPRESSION = NONE)
GO


GO
IF EXISTS (SELECT * FROM sys.objects so JOIN sys.schemas sc ON so.schema_id = sc.schema_id WHERE so.name = N'staff'  AND sc.name = N'dbo'  AND type in (N'U'))
BEGIN

  DECLARE @drop_statement nvarchar(500)

  DECLARE drop_cursor CURSOR FOR
      SELECT 'alter table '+quotename(schema_name(ob.schema_id))+
      '.'+quotename(object_name(ob.object_id))+ ' drop constraint ' + quotename(fk.name) 
      FROM sys.objects ob INNER JOIN sys.foreign_keys fk ON fk.parent_object_id = ob.object_id
      WHERE fk.referenced_object_id = 
          (
             SELECT so.object_id 
             FROM sys.objects so JOIN sys.schemas sc
             ON so.schema_id = sc.schema_id
             WHERE so.name = N'staff'  AND sc.name = N'dbo'  AND type in (N'U')
           )

  OPEN drop_cursor

  FETCH NEXT FROM drop_cursor
  INTO @drop_statement

  WHILE @@FETCH_STATUS = 0
  BEGIN
     EXEC (@drop_statement)

     FETCH NEXT FROM drop_cursor
     INTO @drop_statement
  END

  CLOSE drop_cursor
  DEALLOCATE drop_cursor

  DROP TABLE [staff]
END 
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE 
[staff]
(
   [staff_id] tinyint IDENTITY(1, 1)  NOT NULL,
   [first_name] nvarchar(45)  NOT NULL,
   [last_name] nvarchar(45)  NOT NULL,
   [address_id] int  NOT NULL,
   [picture] varbinary(max)  NULL,
   [email] nvarchar(50)  NULL,
   [store_id] tinyint  NOT NULL,

   /*
   *   SSMA informational messages:
   *   M2SS0052: BIT literal was converted to BINARY literal
   */

   [active] binary(1)  NOT NULL,
   [username] nvarchar(16)  NOT NULL,
   [password] nvarchar(40)  NULL,
   [last_update] datetime  NOT NULL
)
WITH (DATA_COMPRESSION = NONE)
GO



GO
IF EXISTS (SELECT * FROM sys.objects so JOIN sys.schemas sc ON so.schema_id = sc.schema_id WHERE so.name = N'store'  AND sc.name = N'dbo'  AND type in (N'U'))
BEGIN

  DECLARE @drop_statement nvarchar(500)

  DECLARE drop_cursor CURSOR FOR
      SELECT 'alter table '+quotename(schema_name(ob.schema_id))+
      '.'+quotename(object_name(ob.object_id))+ ' drop constraint ' + quotename(fk.name) 
      FROM sys.objects ob INNER JOIN sys.foreign_keys fk ON fk.parent_object_id = ob.object_id
      WHERE fk.referenced_object_id = 
          (
             SELECT so.object_id 
             FROM sys.objects so JOIN sys.schemas sc
             ON so.schema_id = sc.schema_id
             WHERE so.name = N'store'  AND sc.name = N'dbo'  AND type in (N'U')
           )

  OPEN drop_cursor

  FETCH NEXT FROM drop_cursor
  INTO @drop_statement

  WHILE @@FETCH_STATUS = 0
  BEGIN
     EXEC (@drop_statement)

     FETCH NEXT FROM drop_cursor
     INTO @drop_statement
  END

  CLOSE drop_cursor
  DEALLOCATE drop_cursor

  DROP TABLE [store]
END 
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE 
[store]
(
   [store_id] tinyint IDENTITY(1, 1)  NOT NULL,
   [manager_staff_id] tinyint  NOT NULL,
   [address_id] int  NOT NULL,
   [last_update] datetime  NOT NULL
)
WITH (DATA_COMPRESSION = NONE)
GO



GO
IF EXISTS (SELECT * FROM sys.objects so JOIN sys.schemas sc ON so.schema_id = sc.schema_id WHERE so.name = N'PK_actor_actor_id'  AND sc.name = N'dbo'  AND type in (N'PK'))
ALTER TABLE [actor] DROP CONSTRAINT [PK_actor_actor_id]
 GO



ALTER TABLE [actor]
 ADD CONSTRAINT [PK_actor_actor_id]
   PRIMARY KEY
   CLUSTERED ([actor_id] ASC)

GO



GO
IF EXISTS (SELECT * FROM sys.objects so JOIN sys.schemas sc ON so.schema_id = sc.schema_id WHERE so.name = N'PK_address_address_id'  AND sc.name = N'dbo'  AND type in (N'PK'))
ALTER TABLE [address] DROP CONSTRAINT [PK_address_address_id]
 GO



ALTER TABLE [address]
 ADD CONSTRAINT [PK_address_address_id]
   PRIMARY KEY
   CLUSTERED ([address_id] ASC)

GO



GO
IF EXISTS (SELECT * FROM sys.objects so JOIN sys.schemas sc ON so.schema_id = sc.schema_id WHERE so.name = N'PK_category_category_id'  AND sc.name = N'dbo'  AND type in (N'PK'))
ALTER TABLE [category] DROP CONSTRAINT [PK_category_category_id]
 GO



ALTER TABLE [category]
 ADD CONSTRAINT [PK_category_category_id]
   PRIMARY KEY
   CLUSTERED ([category_id] ASC)

GO



GO
IF EXISTS (SELECT * FROM sys.objects so JOIN sys.schemas sc ON so.schema_id = sc.schema_id WHERE so.name = N'PK_city_city_id'  AND sc.name = N'dbo'  AND type in (N'PK'))
ALTER TABLE [city] DROP CONSTRAINT [PK_city_city_id]
 GO



ALTER TABLE [city]
 ADD CONSTRAINT [PK_city_city_id]
   PRIMARY KEY
   CLUSTERED ([city_id] ASC)

GO



GO
IF EXISTS (SELECT * FROM sys.objects so JOIN sys.schemas sc ON so.schema_id = sc.schema_id WHERE so.name = N'PK_country_country_id'  AND sc.name = N'dbo'  AND type in (N'PK'))
ALTER TABLE [country] DROP CONSTRAINT [PK_country_country_id]
 GO



ALTER TABLE [country]
 ADD CONSTRAINT [PK_country_country_id]
   PRIMARY KEY
   CLUSTERED ([country_id] ASC)

GO



GO
IF EXISTS (SELECT * FROM sys.objects so JOIN sys.schemas sc ON so.schema_id = sc.schema_id WHERE so.name = N'PK_customer_customer_id'  AND sc.name = N'dbo'  AND type in (N'PK'))
ALTER TABLE [customer] DROP CONSTRAINT [PK_customer_customer_id]
 GO



ALTER TABLE [customer]
 ADD CONSTRAINT [PK_customer_customer_id]
   PRIMARY KEY
   CLUSTERED ([customer_id] ASC)

GO



GO
IF EXISTS (SELECT * FROM sys.objects so JOIN sys.schemas sc ON so.schema_id = sc.schema_id WHERE so.name = N'PK_databasechangeloglock_ID'  AND sc.name = N'dbo'  AND type in (N'PK'))
ALTER TABLE [databasechangeloglock] DROP CONSTRAINT [PK_databasechangeloglock_ID]
 GO



ALTER TABLE [databasechangeloglock]
 ADD CONSTRAINT [PK_databasechangeloglock_ID]
   PRIMARY KEY
   CLUSTERED ([ID] ASC)

GO



GO
IF EXISTS (SELECT * FROM sys.objects so JOIN sys.schemas sc ON so.schema_id = sc.schema_id WHERE so.name = N'PK_film_film_id'  AND sc.name = N'dbo'  AND type in (N'PK'))
ALTER TABLE [film] DROP CONSTRAINT [PK_film_film_id]
 GO



ALTER TABLE [film]
 ADD CONSTRAINT [PK_film_film_id]
   PRIMARY KEY
   CLUSTERED ([film_id] ASC)

GO



GO
IF EXISTS (SELECT * FROM sys.objects so JOIN sys.schemas sc ON so.schema_id = sc.schema_id WHERE so.name = N'PK_film_actor_actor_id'  AND sc.name = N'dbo'  AND type in (N'PK'))
ALTER TABLE [film_actor] DROP CONSTRAINT [PK_film_actor_actor_id]
 GO



ALTER TABLE [film_actor]
 ADD CONSTRAINT [PK_film_actor_actor_id]
   PRIMARY KEY
   CLUSTERED ([actor_id] ASC, [film_id] ASC)

GO



GO
IF EXISTS (SELECT * FROM sys.objects so JOIN sys.schemas sc ON so.schema_id = sc.schema_id WHERE so.name = N'PK_film_category_film_id'  AND sc.name = N'dbo'  AND type in (N'PK'))
ALTER TABLE [film_category] DROP CONSTRAINT [PK_film_category_film_id]
 GO



ALTER TABLE [film_category]
 ADD CONSTRAINT [PK_film_category_film_id]
   PRIMARY KEY
   CLUSTERED ([film_id] ASC, [category_id] ASC)

GO



GO
IF EXISTS (SELECT * FROM sys.objects so JOIN sys.schemas sc ON so.schema_id = sc.schema_id WHERE so.name = N'PK_film_text_film_id'  AND sc.name = N'dbo'  AND type in (N'PK'))
ALTER TABLE [film_text] DROP CONSTRAINT [PK_film_text_film_id]
 GO



ALTER TABLE [film_text]
 ADD CONSTRAINT [PK_film_text_film_id]
   PRIMARY KEY
   CLUSTERED ([film_id] ASC)

GO



GO
IF EXISTS (SELECT * FROM sys.objects so JOIN sys.schemas sc ON so.schema_id = sc.schema_id WHERE so.name = N'PK_inventory_inventory_id'  AND sc.name = N'dbo'  AND type in (N'PK'))
ALTER TABLE [inventory] DROP CONSTRAINT [PK_inventory_inventory_id]
 GO



ALTER TABLE [inventory]
 ADD CONSTRAINT [PK_inventory_inventory_id]
   PRIMARY KEY
   CLUSTERED ([inventory_id] ASC)

GO



GO
IF EXISTS (SELECT * FROM sys.objects so JOIN sys.schemas sc ON so.schema_id = sc.schema_id WHERE so.name = N'PK_language_language_id'  AND sc.name = N'dbo'  AND type in (N'PK'))
ALTER TABLE [language] DROP CONSTRAINT [PK_language_language_id]
 GO



ALTER TABLE [language]
 ADD CONSTRAINT [PK_language_language_id]
   PRIMARY KEY
   CLUSTERED ([language_id] ASC)

GO



GO
IF EXISTS (SELECT * FROM sys.objects so JOIN sys.schemas sc ON so.schema_id = sc.schema_id WHERE so.name = N'PK_payment_payment_id'  AND sc.name = N'dbo'  AND type in (N'PK'))
ALTER TABLE [payment] DROP CONSTRAINT [PK_payment_payment_id]
 GO



ALTER TABLE [payment]
 ADD CONSTRAINT [PK_payment_payment_id]
   PRIMARY KEY
   CLUSTERED ([payment_id] ASC)

GO



GO
IF EXISTS (SELECT * FROM sys.objects so JOIN sys.schemas sc ON so.schema_id = sc.schema_id WHERE so.name = N'PK_rental_rental_id'  AND sc.name = N'dbo'  AND type in (N'PK'))
ALTER TABLE [rental] DROP CONSTRAINT [PK_rental_rental_id]
 GO



ALTER TABLE [rental]
 ADD CONSTRAINT [PK_rental_rental_id]
   PRIMARY KEY
   CLUSTERED ([rental_id] ASC)

GO





GO
IF EXISTS (SELECT * FROM sys.objects so JOIN sys.schemas sc ON so.schema_id = sc.schema_id WHERE so.name = N'PK_staff_staff_id'  AND sc.name = N'dbo'  AND type in (N'PK'))
ALTER TABLE [staff] DROP CONSTRAINT [PK_staff_staff_id]
 GO



ALTER TABLE [staff]
 ADD CONSTRAINT [PK_staff_staff_id]
   PRIMARY KEY
   CLUSTERED ([staff_id] ASC)

GO



GO
IF EXISTS (SELECT * FROM sys.objects so JOIN sys.schemas sc ON so.schema_id = sc.schema_id WHERE so.name = N'PK_store_store_id'  AND sc.name = N'dbo'  AND type in (N'PK'))
ALTER TABLE [store] DROP CONSTRAINT [PK_store_store_id]
 GO



ALTER TABLE [store]
 ADD CONSTRAINT [PK_store_store_id]
   PRIMARY KEY
   CLUSTERED ([store_id] ASC)

GO



GO
IF EXISTS (SELECT * FROM sys.objects so JOIN sys.schemas sc ON so.schema_id = sc.schema_id WHERE so.name = N'rental$rental_date'  AND sc.name = N'dbo'  AND type in (N'UQ'))
ALTER TABLE [rental] DROP CONSTRAINT [rental$rental_date]
 GO



ALTER TABLE [rental]
 ADD CONSTRAINT [rental$rental_date]
 UNIQUE 
   NONCLUSTERED ([rental_date] ASC, [inventory_id] ASC, [customer_id] ASC)

GO



GO
IF EXISTS (SELECT * FROM sys.objects so JOIN sys.schemas sc ON so.schema_id = sc.schema_id WHERE so.name = N'store$manager_staff_id'  AND sc.name = N'dbo'  AND type in (N'UQ'))
ALTER TABLE [store] DROP CONSTRAINT [store$manager_staff_id]
 GO



ALTER TABLE [store]
 ADD CONSTRAINT [store$manager_staff_id]
 UNIQUE 
   NONCLUSTERED ([manager_staff_id] ASC)

GO



GO
IF EXISTS (SELECT * FROM sys.objects so JOIN sys.schemas sc ON so.schema_id = sc.schema_id 
    WHERE so.name = N'enum2str$film$rating'  AND sc.name=N'dbo'  AND type in (N'FN',N'TF',N'IF',N'FS',N'FT'))
BEGIN

/* Uncomment this block if you have constraints which use this function  

  DECLARE @drop_statement nvarchar(500)
  DECLARE drop_cursor CURSOR FOR
     SELECT
                 'ALTER TABLE ' +
                       quotename(schema_name(tbl.schema_id)) + '.' +
                       quotename(object_name(tbl.object_id)) +
                 ' DROP CONSTRAINT ' + quotename(object_name(constr.object_id))
     FROM sys.sql_expression_dependencies dep
           JOIN sys.objects constr
                 ON constr.object_id = dep.referencing_id AND constr.type = N'C'
           JOIN sys.objects tbl
                 ON tbl.object_id = constr.parent_object_id
     WHERE
           dep.referenced_id =
           (
                 SELECT so.object_id
                       FROM sys.objects so
                             JOIN sys.schemas sc
                                   ON so.schema_id = sc.schema_id
                       WHERE
                             so.name = N'enum2str$film$rating'  AND
                             sc.name=N'dbo'  AND
                             type in (N'FN',N'TF',N'IF')
            )

  OPEN drop_cursor

  FETCH NEXT FROM drop_cursor
  INTO @drop_statement


  WHILE @@FETCH_STATUS = 0
  BEGIN
     EXEC (@drop_statement)
     FETCH NEXT FROM drop_cursor
     INTO @drop_statement
  END

  CLOSE drop_cursor
  DEALLOCATE drop_cursor
*/
  DROP FUNCTION [enum2str$film$rating]
END

GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION enum2str$film$rating 
( 
   @setval tinyint
)
RETURNS nvarchar(max)
AS 
   BEGIN
      RETURN 
         CASE @setval
            WHEN 1 THEN 'G'
            WHEN 2 THEN 'PG'
            WHEN 3 THEN 'PG-13'
            WHEN 4 THEN 'R'
            WHEN 5 THEN 'NC-17'
            ELSE ''
         END
   END
GO



CREATE FUNCTION inventory_in_stock 
( 
   @p_inventory_id int
)
RETURNS smallint
AS 
   BEGIN

      DECLARE
         @v_rentals int

      DECLARE
         @v_out int

      /*
      *   SSMA informational messages:
      *   M2SS0134: Conversion of following Comment(s) is not supported : AN ITEM IS IN-STOCK IF THERE ARE EITHER NO ROWS IN THE rental TABLE
      *   FOR THE ITEM OR ALL ROWS HAVE return_date POPULATED
      *
      */

      SELECT @v_rentals = count_big(*)
      FROM rental
      WHERE rental.inventory_id = @p_inventory_id

      IF @v_rentals = 0
         /*
         *   SSMA informational messages:
         *   M2SS0052: BOOLEAN literal was converted to SMALLINT literal
         */

         RETURN 1

      SELECT @v_out = count_big(rental.rental_id)
      FROM 
         inventory 
            LEFT JOIN rental 
            ON inventory.inventory_id = rental.inventory_id
      WHERE inventory.inventory_id = @p_inventory_id AND rental.return_date IS NULL

      IF @v_out > 0
         /*
         *   SSMA informational messages:
         *   M2SS0052: BOOLEAN literal was converted to SMALLINT literal
         */

         RETURN 0
      ELSE 
         /*
         *   SSMA informational messages:
         *   M2SS0052: BOOLEAN literal was converted to SMALLINT literal
         */

         RETURN 1

      RETURN NULL

   END
GO




GO
IF  EXISTS (SELECT * FROM sys.objects so JOIN sys.schemas sc ON so.schema_id = sc.schema_id WHERE so.name = N'film_in_stock'  AND sc.name=N'dbo'  AND type in (N'P',N'PC'))
 DROP PROCEDURE [film_in_stock]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
*   SSMA informational messages:
*   M2SS0003: The following SQL clause was ignored during conversion:
*   DEFINER = `root`@`%`.
*   M2SS0003: The following SQL clause was ignored during conversion:
*   READS SQL DATA.
*/

CREATE PROCEDURE film_in_stock  
   @p_film_id int,
   @p_store_id int,
   @p_film_count int  OUTPUT
AS 
   BEGIN

      SET  XACT_ABORT  ON

      SET  NOCOUNT  ON

      SET @p_film_count = NULL

      SELECT inventory.inventory_id
      FROM inventory
      WHERE 
         inventory.film_id = @p_film_id AND 
         inventory.store_id = @p_store_id AND 
         dbo.inventory_in_stock(inventory.inventory_id) <> 0

      SELECT @p_film_count = count_big(*)
      FROM inventory
      WHERE 
         inventory.film_id = @p_film_id AND 
         inventory.store_id = @p_store_id AND 
         dbo.inventory_in_stock(inventory.inventory_id) <> 0

   END
GO


GO
IF  EXISTS (SELECT * FROM sys.objects so JOIN sys.schemas sc ON so.schema_id = sc.schema_id WHERE so.name = N'film_not_in_stock'  AND sc.name=N'dbo'  AND type in (N'P',N'PC'))
 DROP PROCEDURE [film_not_in_stock]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
*   SSMA informational messages:
*   M2SS0003: The following SQL clause was ignored during conversion:
*   DEFINER = `root`@`%`.
*   M2SS0003: The following SQL clause was ignored during conversion:
*   READS SQL DATA.
*/

CREATE PROCEDURE film_not_in_stock  
   @p_film_id int,
   @p_store_id int,
   @p_film_count int  OUTPUT
AS 
   BEGIN

      SET  XACT_ABORT  ON

      SET  NOCOUNT  ON

      SET @p_film_count = NULL

      SELECT inventory.inventory_id
      FROM inventory
      WHERE 
         inventory.film_id = @p_film_id AND 
         inventory.store_id = @p_store_id AND 
         NOT dbo.inventory_in_stock(inventory.inventory_id) <> 0

      SELECT @p_film_count = count_big(*)
      FROM inventory
      WHERE 
         inventory.film_id = @p_film_id AND 
         inventory.store_id = @p_store_id AND 
         NOT dbo.inventory_in_stock(inventory.inventory_id) <> 0

   END
GO


GO
IF EXISTS (SELECT * FROM sys.objects so JOIN sys.schemas sc ON so.schema_id = sc.schema_id 
    WHERE so.name = N'get_customer_balance'  AND sc.name=N'dbo'  AND type in (N'FN',N'TF',N'IF',N'FS',N'FT'))
BEGIN

/* Uncomment this block if you have constraints which use this function  

  DECLARE @drop_statement nvarchar(500)
  DECLARE drop_cursor CURSOR FOR
     SELECT
                 'ALTER TABLE ' +
                       quotename(schema_name(tbl.schema_id)) + '.' +
                       quotename(object_name(tbl.object_id)) +
                 ' DROP CONSTRAINT ' + quotename(object_name(constr.object_id))
     FROM sys.sql_expression_dependencies dep
           JOIN sys.objects constr
                 ON constr.object_id = dep.referencing_id AND constr.type = N'C'
           JOIN sys.objects tbl
                 ON tbl.object_id = constr.parent_object_id
     WHERE
           dep.referenced_id =
           (
                 SELECT so.object_id
                       FROM sys.objects so
                             JOIN sys.schemas sc
                                   ON so.schema_id = sc.schema_id
                       WHERE
                             so.name = N'get_customer_balance'  AND
                             sc.name=N'dbo'  AND
                             type in (N'FN',N'TF',N'IF')
            )

  OPEN drop_cursor

  FETCH NEXT FROM drop_cursor
  INTO @drop_statement


  WHILE @@FETCH_STATUS = 0
  BEGIN
     EXEC (@drop_statement)
     FETCH NEXT FROM drop_cursor
     INTO @drop_statement
  END

  CLOSE drop_cursor
  DEALLOCATE drop_cursor
*/
  DROP FUNCTION [get_customer_balance]
END

GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
*   SSMA informational messages:
*   M2SS0003: The following SQL clause was ignored during conversion:
*   DEFINER = `root`@`%`.
*   M2SS0003: The following SQL clause was ignored during conversion:
*   READS SQL DATA.
*   M2SS0003: The following SQL clause was ignored during conversion: DETERMINISTIC.
*   M2SS0134: Conversion of following Comment(s) is not supported : OK, WE NEED TO CALCULATE THE CURRENT BALANCE GIVEN A CUSTOMER_ID AND A DATE
*   THAT WE WANT THE BALANCE TO BE EFFECTIVE FOR. THE BALANCE IS:
*      1) RENTAL FEES FOR ALL PREVIOUS RENTALS
*      2) ONE DOLLAR FOR EVERY DAY THE PREVIOUS RENTALS ARE OVERDUE
*      3) IF A FILM IS MORE THAN RENTAL_DURATION * 2 OVERDUE, CHARGE THE REPLACEMENT_COST
*      4) SUBTRACT ALL PAYMENTS MADE BEFORE THE DATE SPECIFIED
*
*/

CREATE FUNCTION get_customer_balance 
( 
   @p_customer_id int,
   @p_effective_date datetime2(0)
)
RETURNS decimal(5, 2)
AS 
   BEGIN

      /*
      *   SSMA informational messages:
      *   M2SS0134: Conversion of following Comment(s) is not supported : FEES PAID TO RENT THE VIDEOS INITIALLY
      *
      */

      DECLARE
         @v_rentfees decimal(5, 2)

      /*
      *   SSMA informational messages:
      *   M2SS0134: Conversion of following Comment(s) is not supported : LATE FEES FOR PRIOR RENTALS
      *
      */

      DECLARE
         @v_overfees int

      /*
      *   SSMA informational messages:
      *   M2SS0134: Conversion of following Comment(s) is not supported : SUM OF PAYMENTS MADE PREVIOUSLY
      *
      */

      DECLARE
         @v_payments decimal(5, 2)

      SELECT @v_rentfees = ISNULL(sum(film.rental_rate), 0)
      FROM film, inventory, rental
      WHERE 
         film.film_id = inventory.film_id AND 
         inventory.inventory_id = rental.inventory_id AND 
         rental.rental_date <= @p_effective_date AND 
         rental.customer_id = @p_customer_id

      SELECT @v_overfees = ISNULL(sum(
         CASE 
            WHEN (((DATEDIFF(DAY, '1900-01-01', rental.return_date) + 693961) - (DATEDIFF(DAY, '1900-01-01', rental.rental_date) + 693961)) > film.rental_duration) THEN (((DATEDIFF(DAY, '1900-01-01', rental.return_date) + 693961) - (DATEDIFF(DAY, '1900-01-01', rental.rental_date) + 693961)) - film.rental_duration)
            ELSE 0
         END), 0)
      FROM rental, inventory, film
      WHERE 
         film.film_id = inventory.film_id AND 
         inventory.inventory_id = rental.inventory_id AND 
         rental.rental_date <= @p_effective_date AND 
         rental.customer_id = @p_customer_id

      SELECT @v_payments = ISNULL(sum(payment.amount), 0)
      FROM payment
      WHERE payment.payment_date <= @p_effective_date AND payment.customer_id = @p_customer_id

      RETURN @v_rentfees + @v_overfees - @v_payments

   END
GO


GO
IF EXISTS (SELECT * FROM sys.objects so JOIN sys.schemas sc ON so.schema_id = sc.schema_id 
    WHERE so.name = N'inventory_held_by_customer'  AND sc.name=N'dbo'  AND type in (N'FN',N'TF',N'IF',N'FS',N'FT'))
BEGIN

/* Uncomment this block if you have constraints which use this function  

  DECLARE @drop_statement nvarchar(500)
  DECLARE drop_cursor CURSOR FOR
     SELECT
                 'ALTER TABLE ' +
                       quotename(schema_name(tbl.schema_id)) + '.' +
                       quotename(object_name(tbl.object_id)) +
                 ' DROP CONSTRAINT ' + quotename(object_name(constr.object_id))
     FROM sys.sql_expression_dependencies dep
           JOIN sys.objects constr
                 ON constr.object_id = dep.referencing_id AND constr.type = N'C'
           JOIN sys.objects tbl
                 ON tbl.object_id = constr.parent_object_id
     WHERE
           dep.referenced_id =
           (
                 SELECT so.object_id
                       FROM sys.objects so
                             JOIN sys.schemas sc
                                   ON so.schema_id = sc.schema_id
                       WHERE
                             so.name = N'inventory_held_by_customer'  AND
                             sc.name=N'dbo'  AND
                             type in (N'FN',N'TF',N'IF')
            )

  OPEN drop_cursor

  FETCH NEXT FROM drop_cursor
  INTO @drop_statement


  WHILE @@FETCH_STATUS = 0
  BEGIN
     EXEC (@drop_statement)
     FETCH NEXT FROM drop_cursor
     INTO @drop_statement
  END

  CLOSE drop_cursor
  DEALLOCATE drop_cursor
*/
  DROP FUNCTION [inventory_held_by_customer]
END

GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
*   SSMA informational messages:
*   M2SS0003: The following SQL clause was ignored during conversion:
*   DEFINER = `root`@`%`.
*   M2SS0003: The following SQL clause was ignored during conversion:
*   READS SQL DATA.
*/

CREATE FUNCTION inventory_held_by_customer 
( 
   @p_inventory_id int
)
RETURNS int
AS 
   BEGIN

      DECLARE
         @v_customer_id int

      SELECT @v_customer_id = rental.customer_id
      FROM rental
      WHERE rental.return_date IS NULL AND rental.inventory_id = @p_inventory_id

      RETURN @v_customer_id

   END
GO



GO
IF EXISTS (SELECT * FROM sys.objects so JOIN sys.schemas sc ON so.schema_id = sc.schema_id 
    WHERE so.name = N'inventory_in_stock'  AND sc.name=N'dbo'  AND type in (N'FN',N'TF',N'IF',N'FS',N'FT'))
BEGIN

/* Uncomment this block if you have constraints which use this function  

  DECLARE @drop_statement nvarchar(500)
  DECLARE drop_cursor CURSOR FOR
     SELECT
                 'ALTER TABLE ' +
                       quotename(schema_name(tbl.schema_id)) + '.' +
                       quotename(object_name(tbl.object_id)) +
                 ' DROP CONSTRAINT ' + quotename(object_name(constr.object_id))
     FROM sys.sql_expression_dependencies dep
           JOIN sys.objects constr
                 ON constr.object_id = dep.referencing_id AND constr.type = N'C'
           JOIN sys.objects tbl
                 ON tbl.object_id = constr.parent_object_id
     WHERE
           dep.referenced_id =
           (
                 SELECT so.object_id
                       FROM sys.objects so
                             JOIN sys.schemas sc
                                   ON so.schema_id = sc.schema_id
                       WHERE
                             so.name = N'inventory_in_stock'  AND
                             sc.name=N'dbo'  AND
                             type in (N'FN',N'TF',N'IF')
            )

  OPEN drop_cursor

  FETCH NEXT FROM drop_cursor
  INTO @drop_statement


  WHILE @@FETCH_STATUS = 0
  BEGIN
     EXEC (@drop_statement)
     FETCH NEXT FROM drop_cursor
     INTO @drop_statement
  END

  CLOSE drop_cursor
  DEALLOCATE drop_cursor
*/
  DROP FUNCTION [inventory_in_stock]
END

GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
*   SSMA informational messages:
*   M2SS0003: The following SQL clause was ignored during conversion:
*   DEFINER = `root`@`%`.
*   M2SS0003: The following SQL clause was ignored during conversion:
*   READS SQL DATA.
*/



GO
IF EXISTS (SELECT * FROM sys.objects so JOIN sys.schemas sc ON so.schema_id = sc.schema_id 
    WHERE so.name = N'norm_enum$film$rating'  AND sc.name=N'dbo'  AND type in (N'FN',N'TF',N'IF',N'FS',N'FT'))
BEGIN

/* Uncomment this block if you have constraints which use this function  

  DECLARE @drop_statement nvarchar(500)
  DECLARE drop_cursor CURSOR FOR
     SELECT
                 'ALTER TABLE ' +
                       quotename(schema_name(tbl.schema_id)) + '.' +
                       quotename(object_name(tbl.object_id)) +
                 ' DROP CONSTRAINT ' + quotename(object_name(constr.object_id))
     FROM sys.sql_expression_dependencies dep
           JOIN sys.objects constr
                 ON constr.object_id = dep.referencing_id AND constr.type = N'C'
           JOIN sys.objects tbl
                 ON tbl.object_id = constr.parent_object_id
     WHERE
           dep.referenced_id =
           (
                 SELECT so.object_id
                       FROM sys.objects so
                             JOIN sys.schemas sc
                                   ON so.schema_id = sc.schema_id
                       WHERE
                             so.name = N'norm_enum$film$rating'  AND
                             sc.name=N'dbo'  AND
                             type in (N'FN',N'TF',N'IF')
            )

  OPEN drop_cursor

  FETCH NEXT FROM drop_cursor
  INTO @drop_statement


  WHILE @@FETCH_STATUS = 0
  BEGIN
     EXEC (@drop_statement)
     FETCH NEXT FROM drop_cursor
     INTO @drop_statement
  END

  CLOSE drop_cursor
  DEALLOCATE drop_cursor
*/
  DROP FUNCTION [norm_enum$film$rating]
END

GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION norm_enum$film$rating 
( 
   @setval nvarchar(max)
)
RETURNS nvarchar(max)
AS 
   BEGIN
      RETURN dbo.enum2str$film$rating(dbo.str2enum$film$rating(@setval))
   END
GO


GO
IF EXISTS (SELECT * FROM sys.objects so JOIN sys.schemas sc ON so.schema_id = sc.schema_id 
    WHERE so.name = N'norm_set$film$special_features'  AND sc.name=N'dbo'  AND type in (N'FN',N'TF',N'IF',N'FS',N'FT'))
BEGIN

/* Uncomment this block if you have constraints which use this function  

  DECLARE @drop_statement nvarchar(500)
  DECLARE drop_cursor CURSOR FOR
     SELECT
                 'ALTER TABLE ' +
                       quotename(schema_name(tbl.schema_id)) + '.' +
                       quotename(object_name(tbl.object_id)) +
                 ' DROP CONSTRAINT ' + quotename(object_name(constr.object_id))
     FROM sys.sql_expression_dependencies dep
           JOIN sys.objects constr
                 ON constr.object_id = dep.referencing_id AND constr.type = N'C'
           JOIN sys.objects tbl
                 ON tbl.object_id = constr.parent_object_id
     WHERE
           dep.referenced_id =
           (
                 SELECT so.object_id
                       FROM sys.objects so
                             JOIN sys.schemas sc
                                   ON so.schema_id = sc.schema_id
                       WHERE
                             so.name = N'norm_set$film$special_features'  AND
                             sc.name=N'dbo'  AND
                             type in (N'FN',N'TF',N'IF')
            )

  OPEN drop_cursor

  FETCH NEXT FROM drop_cursor
  INTO @drop_statement


  WHILE @@FETCH_STATUS = 0
  BEGIN
     EXEC (@drop_statement)
     FETCH NEXT FROM drop_cursor
     INTO @drop_statement
  END

  CLOSE drop_cursor
  DEALLOCATE drop_cursor
*/
  DROP FUNCTION [norm_set$film$special_features]
END

GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION norm_set$film$special_features 
( 
   @setval varchar(max)
)
RETURNS varchar(max)
AS 
   BEGIN
      RETURN dbo.set2str$film$special_features(dbo.str2set$film$special_features(@setval))
   END
GO



GO
IF  EXISTS (SELECT * FROM sys.objects so JOIN sys.schemas sc ON so.schema_id = sc.schema_id WHERE so.name = N'rewards_report'  AND sc.name=N'dbo'  AND type in (N'P',N'PC'))
 DROP PROCEDURE [rewards_report]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
*   SSMA informational messages:
*   M2SS0003: The following SQL clause was ignored during conversion:
*   DEFINER = `root`@`%`.
*   M2SS0003: The following SQL clause was ignored during conversion:
*   READS SQL DATA.
*   M2SS0003: The following SQL clause was ignored during conversion: COMMENT 'Provides a customizable report on best customers'.
*/

CREATE PROCEDURE rewards_report  
   @min_monthly_purchases tinyint,
   @min_dollar_amount_purchased decimal(10, 2),
   @count_rewardees int  OUTPUT
AS 
   /*
   *   SSMA informational messages:
   *   M2SS0003: The following SQL clause was ignored during conversion:
   *   proc : .
   */

   BEGIN

      SET  XACT_ABORT  ON

      SET  NOCOUNT  ON

      SET @count_rewardees = NULL

      DECLARE
         @last_month_start date

      DECLARE
         @last_month_end date

      /*
      *   SSMA informational messages:
      *   M2SS0134: Conversion of following Comment(s) is not supported :  Some sanity checks... 
      *
      */

      IF @min_monthly_purchases = 0
         BEGIN

            SELECT N'Minimum monthly purchases parameter must be > 0'

            GOTO proc$leave

         END

      IF @min_dollar_amount_purchased = 0.00
         BEGIN

            SELECT N'Minimum monthly dollar amount purchased parameter must be > $0.00'

            GOTO proc$leave

         END

      SET @last_month_start = dateadd(month, -1, CAST(getdate() AS DATE))

      /* 
      *   SSMA error messages:
      *   M2SS0201: MySQL standard function STR_TO_DATE is not supported in current SSMA version

      SET @last_month_start = STR_TO_DATE((CAST(year(@last_month_start) AS varchar(50))) + (N'-') + (CAST(datepart(MONTH, @last_month_start) AS varchar(50))) + (N'-01'), '%Y-%m-%d')
      */



      SET @last_month_end = DATEADD(DAY, -1, DATEADD(MONTH, DATEDIFF(MONTH, CAST('1900-01-01' AS DATE), @last_month_start) + 1, CAST('1900-01-01' AS DATE)))

      CREATE TABLE #tmpCustomer
      (
         customer_id int NOT NULL PRIMARY KEY
      )

      INSERT #tmpCustomer(customer_id)
         SELECT p.customer_id
         FROM payment  AS p
         WHERE p.payment_date BETWEEN @last_month_start AND @last_month_end
         GROUP BY p.customer_id
         HAVING sum(p.amount) > @min_dollar_amount_purchased AND count_big(p.customer_id) > @min_monthly_purchases
            ORDER BY p.customer_id

      /*
      *   SSMA informational messages:
      *   M2SS0134: Conversion of following Comment(s) is not supported :  Populate OUT parameter with count of found customers 
      *
      */

      SELECT @count_rewardees = count_big(*)
      FROM #tmpCustomer

      /*
      *   SSMA informational messages:
      *   M2SS0134: Conversion of following Comment(s) is not supported : 
      *                       Output ALL customer information of matching rewardees.
      *                       Customize output as needed.
      *                   
      *
      */

      SELECT 
         c.customer_id, 
         c.store_id, 
         c.first_name, 
         c.last_name, 
         c.email, 
         c.address_id, 
         c.active, 
         c.create_date, 
         c.last_update
      FROM 
         #tmpCustomer  AS t 
            INNER JOIN customer  AS c 
            ON t.customer_id = c.customer_id

      DROP TABLE #tmpCustomer

   END
   proc$leave:
GO



GO
IF EXISTS (SELECT * FROM sys.objects so JOIN sys.schemas sc ON so.schema_id = sc.schema_id 
    WHERE so.name = N'set2str$film$special_features'  AND sc.name=N'dbo'  AND type in (N'FN',N'TF',N'IF',N'FS',N'FT'))
BEGIN

/* Uncomment this block if you have constraints which use this function  

  DECLARE @drop_statement nvarchar(500)
  DECLARE drop_cursor CURSOR FOR
     SELECT
                 'ALTER TABLE ' +
                       quotename(schema_name(tbl.schema_id)) + '.' +
                       quotename(object_name(tbl.object_id)) +
                 ' DROP CONSTRAINT ' + quotename(object_name(constr.object_id))
     FROM sys.sql_expression_dependencies dep
           JOIN sys.objects constr
                 ON constr.object_id = dep.referencing_id AND constr.type = N'C'
           JOIN sys.objects tbl
                 ON tbl.object_id = constr.parent_object_id
     WHERE
           dep.referenced_id =
           (
                 SELECT so.object_id
                       FROM sys.objects so
                             JOIN sys.schemas sc
                                   ON so.schema_id = sc.schema_id
                       WHERE
                             so.name = N'set2str$film$special_features'  AND
                             sc.name=N'dbo'  AND
                             type in (N'FN',N'TF',N'IF')
            )

  OPEN drop_cursor

  FETCH NEXT FROM drop_cursor
  INTO @drop_statement


  WHILE @@FETCH_STATUS = 0
  BEGIN
     EXEC (@drop_statement)
     FETCH NEXT FROM drop_cursor
     INTO @drop_statement
  END

  CLOSE drop_cursor
  DEALLOCATE drop_cursor
*/
  DROP FUNCTION [set2str$film$special_features]
END

GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION set2str$film$special_features 
( 
   @setval binary(1)
)
RETURNS nvarchar(max)
AS 
   BEGIN

      IF (@setval IS NULL)
         RETURN NULL

      DECLARE
         @rv nvarchar(max)

      SET @rv = ''

      DECLARE
         @setval_bi bigint

      SET @setval_bi = @setval

      IF (@setval_bi & 0x1 = 0x1)
         SET @rv = @rv + ',' + 'Trailers'

      IF (@setval_bi & 0x2 = 0x2)
         SET @rv = @rv + ',' + 'Commentaries'

      IF (@setval_bi & 0x4 = 0x4)
         SET @rv = @rv + ',' + 'Deleted Scenes'

      IF (@setval_bi & 0x8 = 0x8)
         SET @rv = @rv + ',' + 'Behind the Scenes'

      IF (@rv = '')
         RETURN ''

      RETURN SUBSTRING(@rv, 2, LEN(@rv) - 1)

   END
GO



GO
IF EXISTS (SELECT * FROM sys.objects so JOIN sys.schemas sc ON so.schema_id = sc.schema_id 
    WHERE so.name = N'str2enum$film$rating'  AND sc.name=N'dbo'  AND type in (N'FN',N'TF',N'IF',N'FS',N'FT'))
BEGIN

/* Uncomment this block if you have constraints which use this function  

  DECLARE @drop_statement nvarchar(500)
  DECLARE drop_cursor CURSOR FOR
     SELECT
                 'ALTER TABLE ' +
                       quotename(schema_name(tbl.schema_id)) + '.' +
                       quotename(object_name(tbl.object_id)) +
                 ' DROP CONSTRAINT ' + quotename(object_name(constr.object_id))
     FROM sys.sql_expression_dependencies dep
           JOIN sys.objects constr
                 ON constr.object_id = dep.referencing_id AND constr.type = N'C'
           JOIN sys.objects tbl
                 ON tbl.object_id = constr.parent_object_id
     WHERE
           dep.referenced_id =
           (
                 SELECT so.object_id
                       FROM sys.objects so
                             JOIN sys.schemas sc
                                   ON so.schema_id = sc.schema_id
                       WHERE
                             so.name = N'str2enum$film$rating'  AND
                             sc.name=N'dbo'  AND
                             type in (N'FN',N'TF',N'IF')
            )

  OPEN drop_cursor

  FETCH NEXT FROM drop_cursor
  INTO @drop_statement


  WHILE @@FETCH_STATUS = 0
  BEGIN
     EXEC (@drop_statement)
     FETCH NEXT FROM drop_cursor
     INTO @drop_statement
  END

  CLOSE drop_cursor
  DEALLOCATE drop_cursor
*/
  DROP FUNCTION [str2enum$film$rating]
END

GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION str2enum$film$rating 
( 
   @setval nvarchar(max)
)
RETURNS tinyint
AS 
   BEGIN
      RETURN 
         CASE @setval
            WHEN 'G' THEN 1
            WHEN 'PG' THEN 2
            WHEN 'PG-13' THEN 3
            WHEN 'R' THEN 4
            WHEN 'NC-17' THEN 5
            ELSE 0
         END
   END
GO



GO
IF EXISTS (SELECT * FROM sys.objects so JOIN sys.schemas sc ON so.schema_id = sc.schema_id 
    WHERE so.name = N'str2set$film$special_features'  AND sc.name=N'dbo'  AND type in (N'FN',N'TF',N'IF',N'FS',N'FT'))
BEGIN

/* Uncomment this block if you have constraints which use this function  

  DECLARE @drop_statement nvarchar(500)
  DECLARE drop_cursor CURSOR FOR
     SELECT
                 'ALTER TABLE ' +
                       quotename(schema_name(tbl.schema_id)) + '.' +
                       quotename(object_name(tbl.object_id)) +
                 ' DROP CONSTRAINT ' + quotename(object_name(constr.object_id))
     FROM sys.sql_expression_dependencies dep
           JOIN sys.objects constr
                 ON constr.object_id = dep.referencing_id AND constr.type = N'C'
           JOIN sys.objects tbl
                 ON tbl.object_id = constr.parent_object_id
     WHERE
           dep.referenced_id =
           (
                 SELECT so.object_id
                       FROM sys.objects so
                             JOIN sys.schemas sc
                                   ON so.schema_id = sc.schema_id
                       WHERE
                             so.name = N'str2set$film$special_features'  AND
                             sc.name=N'dbo'  AND
                             type in (N'FN',N'TF',N'IF')
            )

  OPEN drop_cursor

  FETCH NEXT FROM drop_cursor
  INTO @drop_statement


  WHILE @@FETCH_STATUS = 0
  BEGIN
     EXEC (@drop_statement)
     FETCH NEXT FROM drop_cursor
     INTO @drop_statement
  END

  CLOSE drop_cursor
  DEALLOCATE drop_cursor
*/
  DROP FUNCTION [str2set$film$special_features]
END

GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION str2set$film$special_features 
( 
   @setval nvarchar(max)
)
RETURNS binary(1)
AS 
   BEGIN

      IF (@setval IS NULL)
         RETURN NULL

      SET @setval = ',' + @setval + ','

      RETURN 
         CASE 
            WHEN @setval LIKE '%,Trailers,%' THEN 0x1
            ELSE CAST(0 AS BIGINT)
         END | 
         CASE 
            WHEN @setval LIKE '%,Commentaries,%' THEN 0x2
            ELSE CAST(0 AS BIGINT)
         END | 
         CASE 
            WHEN @setval LIKE '%,Deleted Scenes,%' THEN 0x4
            ELSE CAST(0 AS BIGINT)
         END | 
         CASE 
            WHEN @setval LIKE '%,Behind the Scenes,%' THEN 0x8
            ELSE CAST(0 AS BIGINT)
         END

   END
GO



GO
IF  EXISTS (select * from sys.objects so join sys.schemas sc on so.schema_id = sc.schema_id where so.name = N'actor_info' and sc.name=N'dbo' AND type in (N'V'))
 DROP VIEW [actor_info]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/* 
*   SSMA error messages:
*   M2SS0201: MySQL standard function GROUP_CONCAT is not supported in current SSMA version
*/

/*
*   SSMA informational messages:
*   M2SS0003: The following SQL clause was ignored during conversion:
*   ALGORITHM =  UNDEFINED.
*   M2SS0003: The following SQL clause was ignored during conversion:
*   DEFINER = `root`@`%`.
*   M2SS0003: The following SQL clause was ignored during conversion:
*   SQL SECURITY DEFINER.
*/

CREATE VIEW actor_info (
   [actor_id], 
   [first_name], 
   [last_name], 
   [film_info])
AS 
   SELECT a.actor_id AS actor_id, a.first_name AS first_name, a.last_name AS last_name, NULL AS film_info
   FROM (((actor  AS a 
      LEFT JOIN film_actor  AS fa 
      ON ((a.actor_id = fa.actor_id))) 
      LEFT JOIN film_category  AS fc 
      ON ((fa.film_id = fc.film_id))) 
      LEFT JOIN category  AS c 
      ON ((fc.category_id = c.category_id)))
   GROUP BY a.actor_id, a.first_name, a.last_name




GO
IF  EXISTS (select * from sys.objects so join sys.schemas sc on so.schema_id = sc.schema_id where so.name = N'customer_list' and sc.name=N'dbo' AND type in (N'V'))
 DROP VIEW [customer_list]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
*   SSMA informational messages:
*   M2SS0003: The following SQL clause was ignored during conversion:
*   ALGORITHM =  UNDEFINED.
*   M2SS0003: The following SQL clause was ignored during conversion:
*   DEFINER = `root`@`%`.
*   M2SS0003: The following SQL clause was ignored during conversion:
*   SQL SECURITY DEFINER.
*/

CREATE VIEW customer_list (
   [ID], 
   [name], 
   [address], 
   [zip code], 
   [phone], 
   [city], 
   [country], 
   [notes], 
   [SID])
AS 
   SELECT 
      cu.customer_id AS ID, 
      cu.first_name + N' ' + cu.last_name AS name, 
      a.address AS address, 
      a.postal_code AS [zip code], 
      a.phone AS phone, 
      city.city AS city, 
      country.country AS country, 
      CASE 
         WHEN (CAST(cu.active AS bigint) <> 0) THEN N'active'
         ELSE N''
      END AS notes, 
      cu.store_id AS SID
   FROM (((customer  AS cu 
      INNER JOIN address  AS a 
      ON ((cu.address_id = a.address_id))) 
      INNER JOIN city 
      ON ((a.city_id = city.city_id))) 
      INNER JOIN country 
      ON ((city.country_id = country.country_id)))
GO



GO
IF  EXISTS (select * from sys.objects so join sys.schemas sc on so.schema_id = sc.schema_id where so.name = N'film_list' and sc.name=N'dbo' AND type in (N'V'))
 DROP VIEW [film_list]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/* 
*   SSMA error messages:
*   M2SS0201: MySQL standard function GROUP_CONCAT is not supported in current SSMA version

/*
*   SSMA informational messages:
*   M2SS0003: The following SQL clause was ignored during conversion:
*   ALGORITHM =  UNDEFINED.
*   M2SS0003: The following SQL clause was ignored during conversion:
*   DEFINER = `root`@`%`.
*   M2SS0003: The following SQL clause was ignored during conversion:
*   SQL SECURITY DEFINER.
*/

CREATE VIEW film_list (
   [FID], 
   [title], 
   [description], 
   [category], 
   [price], 
   [length], 
   [rating], 
   [actors])
AS 
   SELECT 
      film.film_id AS FID, 
      film.title AS title, 
      film.description AS description, 
      category.name AS category, 
      film.rental_rate AS price, 
      film.length AS length, 
      film.rating AS rating, 
      NULL AS actors
   FROM ((((category 
      LEFT JOIN film_category 
      ON ((category.category_id = film_category.category_id))) 
      LEFT JOIN film 
      ON ((film_category.film_id = film.film_id))) 
      INNER JOIN film_actor 
      ON ((film.film_id = film_actor.film_id))) 
      INNER JOIN actor 
      ON ((film_actor.actor_id = actor.actor_id)))
   GROUP BY film.film_id, film.title, film.description, category.name, film.rental_rate, film.length, film.rating





GO
IF  EXISTS (select * from sys.objects so join sys.schemas sc on so.schema_id = sc.schema_id where so.name = N'nicer_but_slower_film_list' and sc.name=N'dbo' AND type in (N'V'))
 DROP VIEW [nicer_but_slower_film_list]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/* 
*   SSMA error messages:
*   M2SS0201: MySQL standard function GROUP_CONCAT is not supported in current SSMA version

/*
*   SSMA informational messages:
*   M2SS0003: The following SQL clause was ignored during conversion:
*   ALGORITHM =  UNDEFINED.
*   M2SS0003: The following SQL clause was ignored during conversion:
*   DEFINER = `root`@`%`.
*   M2SS0003: The following SQL clause was ignored during conversion:
*   SQL SECURITY DEFINER.
*/

CREATE VIEW nicer_but_slower_film_list (
   [FID], 
   [title], 
   [description], 
   [category], 
   [price], 
   [length], 
   [rating], 
   [actors])
AS 
   SELECT 
      film.film_id AS FID, 
      film.title AS title, 
      film.description AS description, 
      category.name AS category, 
      film.rental_rate AS price, 
      film.length AS length, 
      film.rating AS rating, 
      NULL AS actors
   FROM ((((category 
      LEFT JOIN film_category 
      ON ((category.category_id = film_category.category_id))) 
      LEFT JOIN film 
      ON ((film_category.film_id = film.film_id))) 
      INNER JOIN film_actor 
      ON ((film.film_id = film_actor.film_id))) 
      INNER JOIN actor 
      ON ((film_actor.actor_id = actor.actor_id)))
   GROUP BY film.film_id, film.title, film.description, category.name, film.rental_rate, film.length, film.rating





GO
IF  EXISTS (select * from sys.objects so join sys.schemas sc on so.schema_id = sc.schema_id where so.name = N'sales_by_film_category' and sc.name=N'dbo' AND type in (N'V'))
 DROP VIEW [sales_by_film_category]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
*   SSMA informational messages:
*   M2SS0003: The following SQL clause was ignored during conversion:
*   ALGORITHM =  UNDEFINED.
*   M2SS0003: The following SQL clause was ignored during conversion:
*   DEFINER = `root`@`%`.
*   M2SS0003: The following SQL clause was ignored during conversion:
*   SQL SECURITY DEFINER.
*/

CREATE VIEW sales_by_film_category ([category], [total_sales])
AS 
   SELECT TOP (9223372036854775807) c.name AS category, sum(p.amount) AS total_sales
   FROM (((((payment  AS p 
      INNER JOIN rental  AS r 
      ON ((p.rental_id = r.rental_id))) 
      INNER JOIN inventory  AS i 
      ON ((r.inventory_id = i.inventory_id))) 
      INNER JOIN film  AS f 
      ON ((i.film_id = f.film_id))) 
      INNER JOIN film_category  AS fc 
      ON ((f.film_id = fc.film_id))) 
      INNER JOIN category  AS c 
      ON ((fc.category_id = c.category_id)))
   GROUP BY c.name
      ORDER BY total_sales DESC
GO



GO
IF  EXISTS (select * from sys.objects so join sys.schemas sc on so.schema_id = sc.schema_id where so.name = N'sales_by_store' and sc.name=N'dbo' AND type in (N'V'))
 DROP VIEW [sales_by_store]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
*   SSMA informational messages:
*   M2SS0003: The following SQL clause was ignored during conversion:
*   ALGORITHM =  UNDEFINED.
*   M2SS0003: The following SQL clause was ignored during conversion:
*   DEFINER = `root`@`%`.
*   M2SS0003: The following SQL clause was ignored during conversion:
*   SQL SECURITY DEFINER.
*/

CREATE VIEW sales_by_store ([store], [manager], [total_sales])
AS 
   /*
   *   SSMA warning messages:
   *   M2SS0104: Non aggregated column COUNTRY is aggregated with Min(..) in Select, Orderby and Having clauses.
   *   M2SS0104: Non aggregated column CITY is aggregated with Min(..) in Select, Orderby and Having clauses.
   *   M2SS0104: Non aggregated column CITY is aggregated with Min(..) in Select, Orderby and Having clauses.
   *   M2SS0104: Non aggregated column COUNTRY is aggregated with Min(..) in Select, Orderby and Having clauses.
   *   M2SS0104: Non aggregated column FIRST_NAME is aggregated with Min(..) in Select, Orderby and Having clauses.
   *   M2SS0104: Non aggregated column LAST_NAME is aggregated with Min(..) in Select, Orderby and Having clauses.
   */

   SELECT TOP (9223372036854775807) min(c.city) + N',' + min(cy.country) AS store, min(m.first_name) + N' ' + min(m.last_name) AS manager, sum(p.amount) AS total_sales
   FROM (((((((payment  AS p 
      INNER JOIN rental  AS r 
      ON ((p.rental_id = r.rental_id))) 
      INNER JOIN inventory  AS i 
      ON ((r.inventory_id = i.inventory_id))) 
      INNER JOIN store  AS s 
      ON ((i.store_id = s.store_id))) 
      INNER JOIN address  AS a 
      ON ((s.address_id = a.address_id))) 
      INNER JOIN city  AS c 
      ON ((a.city_id = c.city_id))) 
      INNER JOIN country  AS cy 
      ON ((c.country_id = cy.country_id))) 
      INNER JOIN staff  AS m 
      ON ((s.manager_staff_id = m.staff_id)))
   GROUP BY s.store_id
      ORDER BY min(cy.country), min(c.city)
GO


GO
IF  EXISTS (select * from sys.objects so join sys.schemas sc on so.schema_id = sc.schema_id where so.name = N'staff_list' and sc.name=N'dbo' AND type in (N'V'))
 DROP VIEW [staff_list]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
*   SSMA informational messages:
*   M2SS0003: The following SQL clause was ignored during conversion:
*   ALGORITHM =  UNDEFINED.
*   M2SS0003: The following SQL clause was ignored during conversion:
*   DEFINER = `root`@`%`.
*   M2SS0003: The following SQL clause was ignored during conversion:
*   SQL SECURITY DEFINER.
*/

CREATE VIEW staff_list (
   [ID], 
   [name], 
   [address], 
   [zip code], 
   [phone], 
   [city], 
   [country], 
   [SID])
AS 
   SELECT 
      s.staff_id AS ID, 
      s.first_name + N' ' + s.last_name AS name, 
      a.address AS address, 
      a.postal_code AS [zip code], 
      a.phone AS phone, 
      city.city AS city, 
      country.country AS country, 
      s.store_id AS SID
   FROM (((staff  AS s 
      INNER JOIN address  AS a 
      ON ((s.address_id = a.address_id))) 
      INNER JOIN city 
      ON ((a.city_id = city.city_id))) 
      INNER JOIN country 
      ON ((city.country_id = country.country_id)))
GO


GO
IF EXISTS (
       SELECT * FROM sys.objects  so JOIN sys.indexes si
       ON so.object_id = si.object_id
       JOIN sys.schemas sc
       ON so.schema_id = sc.schema_id
       WHERE so.name = N'film_category'  AND sc.name = N'dbo'  AND si.name = N'fk_film_category_category' AND so.type in (N'U'))
   DROP INDEX [fk_film_category_category] ON [film_category] 
GO
CREATE NONCLUSTERED INDEX [fk_film_category_category] ON [film_category]
(
   [category_id] ASC
)
WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY] 
GO
GO


GO
IF EXISTS (
       SELECT * FROM sys.objects  so JOIN sys.indexes si
       ON so.object_id = si.object_id
       JOIN sys.schemas sc
       ON so.schema_id = sc.schema_id
       WHERE so.name = N'payment'  AND sc.name = N'dbo'  AND si.name = N'fk_payment_rental' AND so.type in (N'U'))
   DROP INDEX [fk_payment_rental] ON [payment] 
GO
CREATE NONCLUSTERED INDEX [fk_payment_rental] ON [payment]
(
   [rental_id] ASC
)
WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY] 
GO
GO


GO
IF EXISTS (
       SELECT * FROM sys.objects  so JOIN sys.indexes si
       ON so.object_id = si.object_id
       JOIN sys.schemas sc
       ON so.schema_id = sc.schema_id
       WHERE so.name = N'actor'  AND sc.name = N'dbo'  AND si.name = N'idx_actor_last_name' AND so.type in (N'U'))
   DROP INDEX [idx_actor_last_name] ON [actor] 
GO
CREATE NONCLUSTERED INDEX [idx_actor_last_name] ON [actor]
(
   [last_name] ASC
)
WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY] 
GO
GO


GO
IF EXISTS (
       SELECT * FROM sys.objects  so JOIN sys.indexes si
       ON so.object_id = si.object_id
       JOIN sys.schemas sc
       ON so.schema_id = sc.schema_id
       WHERE so.name = N'staff'  AND sc.name = N'dbo'  AND si.name = N'idx_fk_address_id' AND so.type in (N'U'))
   DROP INDEX [idx_fk_address_id] ON [staff] 
GO
CREATE NONCLUSTERED INDEX [idx_fk_address_id] ON [staff]
(
   [address_id] ASC
)
WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY] 
GO
GO


GO
IF EXISTS (
       SELECT * FROM sys.objects  so JOIN sys.indexes si
       ON so.object_id = si.object_id
       JOIN sys.schemas sc
       ON so.schema_id = sc.schema_id
       WHERE so.name = N'customer'  AND sc.name = N'dbo'  AND si.name = N'idx_fk_address_id' AND so.type in (N'U'))
   DROP INDEX [idx_fk_address_id] ON [customer] 
GO
CREATE NONCLUSTERED INDEX [idx_fk_address_id] ON [customer]
(
   [address_id] ASC
)
WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY] 
GO
GO


GO
IF EXISTS (
       SELECT * FROM sys.objects  so JOIN sys.indexes si
       ON so.object_id = si.object_id
       JOIN sys.schemas sc
       ON so.schema_id = sc.schema_id
       WHERE so.name = N'store'  AND sc.name = N'dbo'  AND si.name = N'idx_fk_address_id' AND so.type in (N'U'))
   DROP INDEX [idx_fk_address_id] ON [store] 
GO
CREATE NONCLUSTERED INDEX [idx_fk_address_id] ON [store]
(
   [address_id] ASC
)
WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY] 
GO
GO


GO
IF EXISTS (
       SELECT * FROM sys.objects  so JOIN sys.indexes si
       ON so.object_id = si.object_id
       JOIN sys.schemas sc
       ON so.schema_id = sc.schema_id
       WHERE so.name = N'address'  AND sc.name = N'dbo'  AND si.name = N'idx_fk_city_id' AND so.type in (N'U'))
   DROP INDEX [idx_fk_city_id] ON [address] 
GO
CREATE NONCLUSTERED INDEX [idx_fk_city_id] ON [address]
(
   [city_id] ASC
)
WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY] 
GO
GO


GO
IF EXISTS (
       SELECT * FROM sys.objects  so JOIN sys.indexes si
       ON so.object_id = si.object_id
       JOIN sys.schemas sc
       ON so.schema_id = sc.schema_id
       WHERE so.name = N'city'  AND sc.name = N'dbo'  AND si.name = N'idx_fk_country_id' AND so.type in (N'U'))
   DROP INDEX [idx_fk_country_id] ON [city] 
GO
CREATE NONCLUSTERED INDEX [idx_fk_country_id] ON [city]
(
   [country_id] ASC
)
WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY] 
GO
GO


GO
IF EXISTS (
       SELECT * FROM sys.objects  so JOIN sys.indexes si
       ON so.object_id = si.object_id
       JOIN sys.schemas sc
       ON so.schema_id = sc.schema_id
       WHERE so.name = N'payment'  AND sc.name = N'dbo'  AND si.name = N'idx_fk_customer_id' AND so.type in (N'U'))
   DROP INDEX [idx_fk_customer_id] ON [payment] 
GO
CREATE NONCLUSTERED INDEX [idx_fk_customer_id] ON [payment]
(
   [customer_id] ASC
)
WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY] 
GO
GO


GO
IF EXISTS (
       SELECT * FROM sys.objects  so JOIN sys.indexes si
       ON so.object_id = si.object_id
       JOIN sys.schemas sc
       ON so.schema_id = sc.schema_id
       WHERE so.name = N'rental'  AND sc.name = N'dbo'  AND si.name = N'idx_fk_customer_id' AND so.type in (N'U'))
   DROP INDEX [idx_fk_customer_id] ON [rental] 
GO
CREATE NONCLUSTERED INDEX [idx_fk_customer_id] ON [rental]
(
   [customer_id] ASC
)
WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY] 
GO
GO


GO
IF EXISTS (
       SELECT * FROM sys.objects  so JOIN sys.indexes si
       ON so.object_id = si.object_id
       JOIN sys.schemas sc
       ON so.schema_id = sc.schema_id
       WHERE so.name = N'inventory'  AND sc.name = N'dbo'  AND si.name = N'idx_fk_film_id' AND so.type in (N'U'))
   DROP INDEX [idx_fk_film_id] ON [inventory] 
GO
CREATE NONCLUSTERED INDEX [idx_fk_film_id] ON [inventory]
(
   [film_id] ASC
)
WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY] 
GO
GO


GO
IF EXISTS (
       SELECT * FROM sys.objects  so JOIN sys.indexes si
       ON so.object_id = si.object_id
       JOIN sys.schemas sc
       ON so.schema_id = sc.schema_id
       WHERE so.name = N'film_actor'  AND sc.name = N'dbo'  AND si.name = N'idx_fk_film_id' AND so.type in (N'U'))
   DROP INDEX [idx_fk_film_id] ON [film_actor] 
GO
CREATE NONCLUSTERED INDEX [idx_fk_film_id] ON [film_actor]
(
   [film_id] ASC
)
WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY] 
GO
GO


GO
IF EXISTS (
       SELECT * FROM sys.objects  so JOIN sys.indexes si
       ON so.object_id = si.object_id
       JOIN sys.schemas sc
       ON so.schema_id = sc.schema_id
       WHERE so.name = N'rental'  AND sc.name = N'dbo'  AND si.name = N'idx_fk_inventory_id' AND so.type in (N'U'))
   DROP INDEX [idx_fk_inventory_id] ON [rental] 
GO
CREATE NONCLUSTERED INDEX [idx_fk_inventory_id] ON [rental]
(
   [inventory_id] ASC
)
WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY] 
GO
GO


GO
IF EXISTS (
       SELECT * FROM sys.objects  so JOIN sys.indexes si
       ON so.object_id = si.object_id
       JOIN sys.schemas sc
       ON so.schema_id = sc.schema_id
       WHERE so.name = N'film'  AND sc.name = N'dbo'  AND si.name = N'idx_fk_language_id' AND so.type in (N'U'))
   DROP INDEX [idx_fk_language_id] ON [film] 
GO
CREATE NONCLUSTERED INDEX [idx_fk_language_id] ON [film]
(
   [language_id] ASC
)
WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY] 
GO
GO


GO
IF EXISTS (
       SELECT * FROM sys.objects  so JOIN sys.indexes si
       ON so.object_id = si.object_id
       JOIN sys.schemas sc
       ON so.schema_id = sc.schema_id
       WHERE so.name = N'film'  AND sc.name = N'dbo'  AND si.name = N'idx_fk_original_language_id' AND so.type in (N'U'))
   DROP INDEX [idx_fk_original_language_id] ON [film] 
GO
CREATE NONCLUSTERED INDEX [idx_fk_original_language_id] ON [film]
(
   [original_language_id] ASC
)
WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY] 
GO
GO


GO
IF EXISTS (
       SELECT * FROM sys.objects  so JOIN sys.indexes si
       ON so.object_id = si.object_id
       JOIN sys.schemas sc
       ON so.schema_id = sc.schema_id
       WHERE so.name = N'payment'  AND sc.name = N'dbo'  AND si.name = N'idx_fk_staff_id' AND so.type in (N'U'))
   DROP INDEX [idx_fk_staff_id] ON [payment] 
GO
CREATE NONCLUSTERED INDEX [idx_fk_staff_id] ON [payment]
(
   [staff_id] ASC
)
WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY] 
GO
GO


GO
IF EXISTS (
       SELECT * FROM sys.objects  so JOIN sys.indexes si
       ON so.object_id = si.object_id
       JOIN sys.schemas sc
       ON so.schema_id = sc.schema_id
       WHERE so.name = N'rental'  AND sc.name = N'dbo'  AND si.name = N'idx_fk_staff_id' AND so.type in (N'U'))
   DROP INDEX [idx_fk_staff_id] ON [rental] 
GO
CREATE NONCLUSTERED INDEX [idx_fk_staff_id] ON [rental]
(
   [staff_id] ASC
)
WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY] 
GO
GO


GO
IF EXISTS (
       SELECT * FROM sys.objects  so JOIN sys.indexes si
       ON so.object_id = si.object_id
       JOIN sys.schemas sc
       ON so.schema_id = sc.schema_id
       WHERE so.name = N'customer'  AND sc.name = N'dbo'  AND si.name = N'idx_fk_store_id' AND so.type in (N'U'))
   DROP INDEX [idx_fk_store_id] ON [customer] 
GO
CREATE NONCLUSTERED INDEX [idx_fk_store_id] ON [customer]
(
   [store_id] ASC
)
WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY] 
GO
GO


GO
IF EXISTS (
       SELECT * FROM sys.objects  so JOIN sys.indexes si
       ON so.object_id = si.object_id
       JOIN sys.schemas sc
       ON so.schema_id = sc.schema_id
       WHERE so.name = N'staff'  AND sc.name = N'dbo'  AND si.name = N'idx_fk_store_id' AND so.type in (N'U'))
   DROP INDEX [idx_fk_store_id] ON [staff] 
GO
CREATE NONCLUSTERED INDEX [idx_fk_store_id] ON [staff]
(
   [store_id] ASC
)
WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY] 
GO
GO


GO
IF EXISTS (
       SELECT * FROM sys.objects  so JOIN sys.indexes si
       ON so.object_id = si.object_id
       JOIN sys.schemas sc
       ON so.schema_id = sc.schema_id
       WHERE so.name = N'customer'  AND sc.name = N'dbo'  AND si.name = N'idx_last_name' AND so.type in (N'U'))
   DROP INDEX [idx_last_name] ON [customer] 
GO
CREATE NONCLUSTERED INDEX [idx_last_name] ON [customer]
(
   [last_name] ASC
)
WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY] 
GO
GO


GO
IF EXISTS (
       SELECT * FROM sys.objects  so JOIN sys.indexes si
       ON so.object_id = si.object_id
       JOIN sys.schemas sc
       ON so.schema_id = sc.schema_id
       WHERE so.name = N'inventory'  AND sc.name = N'dbo'  AND si.name = N'idx_store_id_film_id' AND so.type in (N'U'))
   DROP INDEX [idx_store_id_film_id] ON [inventory] 
GO
CREATE NONCLUSTERED INDEX [idx_store_id_film_id] ON [inventory]
(
   [store_id] ASC,
   [film_id] ASC
)
WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY] 
GO
GO


GO
IF EXISTS (
       SELECT * FROM sys.objects  so JOIN sys.indexes si
       ON so.object_id = si.object_id
       JOIN sys.schemas sc
       ON so.schema_id = sc.schema_id
       WHERE so.name = N'film'  AND sc.name = N'dbo'  AND si.name = N'idx_title' AND so.type in (N'U'))
   DROP INDEX [idx_title] ON [film] 
GO
CREATE NONCLUSTERED INDEX [idx_title] ON [film]
(
   [title] ASC
)
WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY] 
GO
GO


GO
IF EXISTS (SELECT * FROM sys.objects so JOIN sys.schemas sc ON so.schema_id = sc.schema_id WHERE so.name = N'address$fk_address_city'  AND sc.name = N'dbo'  AND type in (N'F'))
ALTER TABLE [address] DROP CONSTRAINT [address$fk_address_city]
 GO



ALTER TABLE [address]
 ADD CONSTRAINT [address$fk_address_city]
 FOREIGN KEY 
   ([city_id])
 REFERENCES 
   [city]     ([city_id])
    ON DELETE NO ACTION
    ON UPDATE CASCADE

GO



GO
IF EXISTS (SELECT * FROM sys.objects so JOIN sys.schemas sc ON so.schema_id = sc.schema_id WHERE so.name = N'city$fk_city_country'  AND sc.name = N'dbo'  AND type in (N'F'))
ALTER TABLE [city] DROP CONSTRAINT [city$fk_city_country]
 GO



ALTER TABLE [city]
 ADD CONSTRAINT [city$fk_city_country]
 FOREIGN KEY 
   ([country_id])
 REFERENCES 
   [country]     ([country_id])
    ON DELETE NO ACTION
    ON UPDATE CASCADE

GO



GO
IF EXISTS (SELECT * FROM sys.objects so JOIN sys.schemas sc ON so.schema_id = sc.schema_id WHERE so.name = N'customer$fk_customer_address'  AND sc.name = N'dbo'  AND type in (N'F'))
ALTER TABLE [customer] DROP CONSTRAINT [customer$fk_customer_address]
 GO



ALTER TABLE [customer]
 ADD CONSTRAINT [customer$fk_customer_address]
 FOREIGN KEY 
   ([address_id])
 REFERENCES 
   [address]     ([address_id])
    ON DELETE NO ACTION
    ON UPDATE CASCADE

GO

IF EXISTS (SELECT * FROM sys.objects so JOIN sys.schemas sc ON so.schema_id = sc.schema_id WHERE so.name = N'customer$fk_customer_store'  AND sc.name = N'dbo'  AND type in (N'F'))
ALTER TABLE [customer] DROP CONSTRAINT [customer$fk_customer_store]
 GO


/* 
*   SSMA error messages:
*   M2SS0037: ON UPDATE CASCADE|SET NULL|SET DEFAULT action was changed to NO ACTION to avoid multiple paths in cascaded foreign keys.
*/


ALTER TABLE [customer]
 ADD CONSTRAINT [customer$fk_customer_store]
 FOREIGN KEY 
   ([store_id])
 REFERENCES 
   [store]     ([store_id])
    ON DELETE NO ACTION
    ON UPDATE NO ACTION

GO



GO
IF EXISTS (SELECT * FROM sys.objects so JOIN sys.schemas sc ON so.schema_id = sc.schema_id WHERE so.name = N'film$fk_film_language'  AND sc.name = N'dbo'  AND type in (N'F'))
ALTER TABLE [film] DROP CONSTRAINT [film$fk_film_language]
 GO


/* 
*   SSMA error messages:
*   M2SS0037: ON UPDATE CASCADE|SET NULL|SET DEFAULT action was changed to NO ACTION to avoid multiple paths in cascaded foreign keys.
*/


ALTER TABLE [film]
 ADD CONSTRAINT [film$fk_film_language]
 FOREIGN KEY 
   ([language_id])
 REFERENCES 
   [language]     ([language_id])
    ON DELETE NO ACTION
    ON UPDATE NO ACTION

GO

IF EXISTS (SELECT * FROM sys.objects so JOIN sys.schemas sc ON so.schema_id = sc.schema_id WHERE so.name = N'film$fk_film_language_original'  AND sc.name = N'dbo'  AND type in (N'F'))
ALTER TABLE [film] DROP CONSTRAINT [film$fk_film_language_original]
 GO



ALTER TABLE [film]
 ADD CONSTRAINT [film$fk_film_language_original]
 FOREIGN KEY 
   ([original_language_id])
 REFERENCES 
   [language]     ([language_id])
    ON DELETE NO ACTION
    ON UPDATE CASCADE

GO



GO
IF EXISTS (SELECT * FROM sys.objects so JOIN sys.schemas sc ON so.schema_id = sc.schema_id WHERE so.name = N'film_actor$fk_film_actor_actor'  AND sc.name = N'dbo'  AND type in (N'F'))
ALTER TABLE [film_actor] DROP CONSTRAINT [film_actor$fk_film_actor_actor]
 GO



ALTER TABLE [film_actor]
 ADD CONSTRAINT [film_actor$fk_film_actor_actor]
 FOREIGN KEY 
   ([actor_id])
 REFERENCES 
   [actor]     ([actor_id])
    ON DELETE NO ACTION
    ON UPDATE CASCADE

GO

IF EXISTS (SELECT * FROM sys.objects so JOIN sys.schemas sc ON so.schema_id = sc.schema_id WHERE so.name = N'film_actor$fk_film_actor_film'  AND sc.name = N'dbo'  AND type in (N'F'))
ALTER TABLE [film_actor] DROP CONSTRAINT [film_actor$fk_film_actor_film]
 GO



ALTER TABLE [film_actor]
 ADD CONSTRAINT [film_actor$fk_film_actor_film]
 FOREIGN KEY 
   ([film_id])
 REFERENCES 
   [film]     ([film_id])
    ON DELETE NO ACTION
    ON UPDATE CASCADE

GO



GO
IF EXISTS (SELECT * FROM sys.objects so JOIN sys.schemas sc ON so.schema_id = sc.schema_id WHERE so.name = N'film_category$fk_film_category_category'  AND sc.name = N'dbo'  AND type in (N'F'))
ALTER TABLE [film_category] DROP CONSTRAINT [film_category$fk_film_category_category]
 GO



ALTER TABLE [film_category]
 ADD CONSTRAINT [film_category$fk_film_category_category]
 FOREIGN KEY 
   ([category_id])
 REFERENCES 
   [category]     ([category_id])
    ON DELETE NO ACTION
    ON UPDATE CASCADE

GO

IF EXISTS (SELECT * FROM sys.objects so JOIN sys.schemas sc ON so.schema_id = sc.schema_id WHERE so.name = N'film_category$fk_film_category_film'  AND sc.name = N'dbo'  AND type in (N'F'))
ALTER TABLE [film_category] DROP CONSTRAINT [film_category$fk_film_category_film]
 GO



ALTER TABLE [film_category]
 ADD CONSTRAINT [film_category$fk_film_category_film]
 FOREIGN KEY 
   ([film_id])
 REFERENCES 
   [film]     ([film_id])
    ON DELETE NO ACTION
    ON UPDATE CASCADE

GO



GO
IF EXISTS (SELECT * FROM sys.objects so JOIN sys.schemas sc ON so.schema_id = sc.schema_id WHERE so.name = N'inventory$fk_inventory_film'  AND sc.name = N'dbo'  AND type in (N'F'))
ALTER TABLE [inventory] DROP CONSTRAINT [inventory$fk_inventory_film]
 GO



ALTER TABLE [inventory]
 ADD CONSTRAINT [inventory$fk_inventory_film]
 FOREIGN KEY 
   ([film_id])
 REFERENCES 
   [film]     ([film_id])
    ON DELETE NO ACTION
    ON UPDATE CASCADE

GO

IF EXISTS (SELECT * FROM sys.objects so JOIN sys.schemas sc ON so.schema_id = sc.schema_id WHERE so.name = N'inventory$fk_inventory_store'  AND sc.name = N'dbo'  AND type in (N'F'))
ALTER TABLE [inventory] DROP CONSTRAINT [inventory$fk_inventory_store]
 GO



ALTER TABLE [inventory]
 ADD CONSTRAINT [inventory$fk_inventory_store]
 FOREIGN KEY 
   ([store_id])
 REFERENCES 
   [store]     ([store_id])
    ON DELETE NO ACTION
    ON UPDATE CASCADE

GO



GO
IF EXISTS (SELECT * FROM sys.objects so JOIN sys.schemas sc ON so.schema_id = sc.schema_id WHERE so.name = N'payment$fk_payment_customer'  AND sc.name = N'dbo'  AND type in (N'F'))
ALTER TABLE [payment] DROP CONSTRAINT [payment$fk_payment_customer]
 GO



ALTER TABLE [payment]
 ADD CONSTRAINT [payment$fk_payment_customer]
 FOREIGN KEY 
   ([customer_id])
 REFERENCES 
   [customer]     ([customer_id])
    ON DELETE NO ACTION
    ON UPDATE CASCADE

GO

IF EXISTS (SELECT * FROM sys.objects so JOIN sys.schemas sc ON so.schema_id = sc.schema_id WHERE so.name = N'payment$fk_payment_rental'  AND sc.name = N'dbo'  AND type in (N'F'))
ALTER TABLE [payment] DROP CONSTRAINT [payment$fk_payment_rental]
 GO


/* 
*   SSMA error messages:
*   M2SS0037: ON UPDATE CASCADE|SET NULL|SET DEFAULT action was changed to NO ACTION to avoid multiple paths in cascaded foreign keys.
*/


ALTER TABLE [payment]
 ADD CONSTRAINT [payment$fk_payment_rental]
 FOREIGN KEY 
   ([rental_id])
 REFERENCES 
   [rental]     ([rental_id])
    ON DELETE SET NULL
    ON UPDATE NO ACTION

GO

IF EXISTS (SELECT * FROM sys.objects so JOIN sys.schemas sc ON so.schema_id = sc.schema_id WHERE so.name = N'payment$fk_payment_staff'  AND sc.name = N'dbo'  AND type in (N'F'))
ALTER TABLE [payment] DROP CONSTRAINT [payment$fk_payment_staff]
 GO


/* 
*   SSMA error messages:
*   M2SS0037: ON UPDATE CASCADE|SET NULL|SET DEFAULT action was changed to NO ACTION to avoid multiple paths in cascaded foreign keys.
*/


ALTER TABLE [payment]
 ADD CONSTRAINT [payment$fk_payment_staff]
 FOREIGN KEY 
   ([staff_id])
 REFERENCES 
   [staff]     ([staff_id])
    ON DELETE NO ACTION
    ON UPDATE NO ACTION

GO



GO
IF EXISTS (SELECT * FROM sys.objects so JOIN sys.schemas sc ON so.schema_id = sc.schema_id WHERE so.name = N'rental$fk_rental_customer'  AND sc.name = N'dbo'  AND type in (N'F'))
ALTER TABLE [rental] DROP CONSTRAINT [rental$fk_rental_customer]
 GO



ALTER TABLE [rental]
 ADD CONSTRAINT [rental$fk_rental_customer]
 FOREIGN KEY 
   ([customer_id])
 REFERENCES 
   [customer]     ([customer_id])
    ON DELETE NO ACTION
    ON UPDATE CASCADE

GO

IF EXISTS (SELECT * FROM sys.objects so JOIN sys.schemas sc ON so.schema_id = sc.schema_id WHERE so.name = N'rental$fk_rental_inventory'  AND sc.name = N'dbo'  AND type in (N'F'))
ALTER TABLE [rental] DROP CONSTRAINT [rental$fk_rental_inventory]
 GO


/* 
*   SSMA error messages:
*   M2SS0037: ON UPDATE CASCADE|SET NULL|SET DEFAULT action was changed to NO ACTION to avoid multiple paths in cascaded foreign keys.
*/


ALTER TABLE [rental]
 ADD CONSTRAINT [rental$fk_rental_inventory]
 FOREIGN KEY 
   ([inventory_id])
 REFERENCES 
   [inventory]     ([inventory_id])
    ON DELETE NO ACTION
    ON UPDATE NO ACTION

GO

IF EXISTS (SELECT * FROM sys.objects so JOIN sys.schemas sc ON so.schema_id = sc.schema_id WHERE so.name = N'rental$fk_rental_staff'  AND sc.name = N'dbo'  AND type in (N'F'))
ALTER TABLE [rental] DROP CONSTRAINT [rental$fk_rental_staff]
 GO


/* 
*   SSMA error messages:
*   M2SS0037: ON UPDATE CASCADE|SET NULL|SET DEFAULT action was changed to NO ACTION to avoid multiple paths in cascaded foreign keys.
*/


ALTER TABLE [rental]
 ADD CONSTRAINT [rental$fk_rental_staff]
 FOREIGN KEY 
   ([staff_id])
 REFERENCES 
   [staff]     ([staff_id])
    ON DELETE NO ACTION
    ON UPDATE NO ACTION

GO



GO
IF EXISTS (SELECT * FROM sys.objects so JOIN sys.schemas sc ON so.schema_id = sc.schema_id WHERE so.name = N'staff$fk_staff_address'  AND sc.name = N'dbo'  AND type in (N'F'))
ALTER TABLE [staff] DROP CONSTRAINT [staff$fk_staff_address]
 GO



ALTER TABLE [staff]
 ADD CONSTRAINT [staff$fk_staff_address]
 FOREIGN KEY 
   ([address_id])
 REFERENCES 
   [address]     ([address_id])
    ON DELETE NO ACTION
    ON UPDATE CASCADE

GO

IF EXISTS (SELECT * FROM sys.objects so JOIN sys.schemas sc ON so.schema_id = sc.schema_id WHERE so.name = N'staff$fk_staff_store'  AND sc.name = N'dbo'  AND type in (N'F'))
ALTER TABLE [staff] DROP CONSTRAINT [staff$fk_staff_store]
 GO


/* 
*   SSMA error messages:
*   M2SS0036: ON UPDATE CASCADE|SET NULL|SET DEFAULT action  was changed to NO ACTION to avoid circular references of cascaded foreign keys.
*/


ALTER TABLE [staff]
 ADD CONSTRAINT [staff$fk_staff_store]
 FOREIGN KEY 
   ([store_id])
 REFERENCES 
   [store]     ([store_id])
    ON DELETE NO ACTION
    ON UPDATE NO ACTION

GO



GO
IF EXISTS (SELECT * FROM sys.objects so JOIN sys.schemas sc ON so.schema_id = sc.schema_id WHERE so.name = N'store$fk_store_address'  AND sc.name = N'dbo'  AND type in (N'F'))
ALTER TABLE [store] DROP CONSTRAINT [store$fk_store_address]
 GO



ALTER TABLE [store]
 ADD CONSTRAINT [store$fk_store_address]
 FOREIGN KEY 
   ([address_id])
 REFERENCES 
   [address]     ([address_id])
    ON DELETE NO ACTION
    ON UPDATE CASCADE

GO

IF EXISTS (SELECT * FROM sys.objects so JOIN sys.schemas sc ON so.schema_id = sc.schema_id WHERE so.name = N'store$fk_store_staff'  AND sc.name = N'dbo'  AND type in (N'F'))
ALTER TABLE [store] DROP CONSTRAINT [store$fk_store_staff]
 GO


/* 
*   SSMA error messages:
*   M2SS0036: ON UPDATE CASCADE|SET NULL|SET DEFAULT action  was changed to NO ACTION to avoid circular references of cascaded foreign keys.
*/


ALTER TABLE [store]
 ADD CONSTRAINT [store$fk_store_staff]
 FOREIGN KEY 
   ([manager_staff_id])
 REFERENCES 
   [staff]     ([staff_id])
    ON DELETE NO ACTION
    ON UPDATE NO ACTION

GO



GO
ALTER TABLE  [actor]
 ADD DEFAULT getdate() FOR [last_update]
GO



GO
ALTER TABLE  [address]
 ADD DEFAULT NULL FOR [address2]
GO

ALTER TABLE  [address]
 ADD DEFAULT NULL FOR [postal_code]
GO

ALTER TABLE  [address]
 ADD DEFAULT getdate() FOR [last_update]
GO



GO
ALTER TABLE  [category]
 ADD DEFAULT getdate() FOR [last_update]
GO



GO
ALTER TABLE  [city]
 ADD DEFAULT getdate() FOR [last_update]
GO



GO
ALTER TABLE  [country]
 ADD DEFAULT getdate() FOR [last_update]
GO



GO
ALTER TABLE  [customer]
 ADD DEFAULT NULL FOR [email]
GO

ALTER TABLE  [customer]
 ADD DEFAULT 0x1 FOR [active]
GO

ALTER TABLE  [customer]
 ADD DEFAULT getdate() FOR [last_update]
GO



GO
ALTER TABLE  [databasechangelog]
 ADD DEFAULT NULL FOR [MD5SUM]
GO

ALTER TABLE  [databasechangelog]
 ADD DEFAULT NULL FOR [DESCRIPTION]
GO

ALTER TABLE  [databasechangelog]
 ADD DEFAULT NULL FOR [COMMENTS]
GO

ALTER TABLE  [databasechangelog]
 ADD DEFAULT NULL FOR [TAG]
GO

ALTER TABLE  [databasechangelog]
 ADD DEFAULT NULL FOR [LIQUIBASE]
GO

ALTER TABLE  [databasechangelog]
 ADD DEFAULT NULL FOR [CONTEXTS]
GO

ALTER TABLE  [databasechangelog]
 ADD DEFAULT NULL FOR [LABELS]
GO

ALTER TABLE  [databasechangelog]
 ADD DEFAULT NULL FOR [DEPLOYMENT_ID]
GO



GO
ALTER TABLE  [databasechangeloglock]
 ADD DEFAULT NULL FOR [LOCKGRANTED]
GO

ALTER TABLE  [databasechangeloglock]
 ADD DEFAULT NULL FOR [LOCKEDBY]
GO



GO
ALTER TABLE  [film]
 ADD DEFAULT NULL FOR [release_year]
GO

ALTER TABLE  [film]
 ADD DEFAULT NULL FOR [original_language_id]
GO

ALTER TABLE  [film]
 ADD DEFAULT 3 FOR [rental_duration]
GO

ALTER TABLE  [film]
 ADD DEFAULT 4.99 FOR [rental_rate]
GO

ALTER TABLE  [film]
 ADD DEFAULT NULL FOR [length]
GO

ALTER TABLE  [film]
 ADD DEFAULT 19.99 FOR [replacement_cost]
GO

ALTER TABLE  [film]
 ADD DEFAULT N'G' FOR [rating]
GO

ALTER TABLE  [film]
 ADD DEFAULT NULL FOR [special_features]
GO

ALTER TABLE  [film]
 ADD DEFAULT getdate() FOR [last_update]
GO



GO
ALTER TABLE  [film_actor]
 ADD DEFAULT getdate() FOR [last_update]
GO



GO
ALTER TABLE  [film_category]
 ADD DEFAULT getdate() FOR [last_update]
GO



GO
ALTER TABLE  [inventory]
 ADD DEFAULT getdate() FOR [last_update]
GO



GO
ALTER TABLE  [language]
 ADD DEFAULT getdate() FOR [last_update]
GO



GO
ALTER TABLE  [payment]
 ADD DEFAULT NULL FOR [rental_id]
GO

ALTER TABLE  [payment]
 ADD DEFAULT getdate() FOR [last_update]
GO



GO
ALTER TABLE  [rental]
 ADD DEFAULT NULL FOR [return_date]
GO

ALTER TABLE  [rental]
 ADD DEFAULT getdate() FOR [last_update]
GO






GO
ALTER TABLE  [staff]
 ADD DEFAULT NULL FOR [email]
GO

ALTER TABLE  [staff]
 ADD DEFAULT 0x1 FOR [active]
GO

ALTER TABLE  [staff]
 ADD DEFAULT NULL FOR [password]
GO

ALTER TABLE  [staff]
 ADD DEFAULT getdate() FOR [last_update]
GO



GO
ALTER TABLE  [store]
 ADD DEFAULT getdate() FOR [last_update]
GO

