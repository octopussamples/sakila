

/****** Object:  UserDefinedFunction [dbo].[enum2str$film$rating]    Script Date: 10/27/2020 3:12:12 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[enum2str$film$rating] 
( 
   @setval tinyint
)
RETURNS nvarchar(max)
AS 
   BEGIN
      RETURN 
         CASE @setval
            WHEN 1 THEN 'G'
            WHEN 2 THEN 'PG'
            WHEN 3 THEN 'PG-13'
            WHEN 4 THEN 'R'
            WHEN 5 THEN 'NC-17'
            ELSE ''
         END
   END

GO


/****** Object:  UserDefinedFunction [dbo].[get_customer_balance]    Script Date: 10/27/2020 3:13:11 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[get_customer_balance] 
( 
   @p_customer_id INT,
   @p_effective_date DATETIME2(0)
)
RETURNS DECIMAL(5, 2)
AS 
   BEGIN

      /*
      *   SSMA informational messages:
      *   M2SS0134: Conversion of following Comment(s) is not supported : FEES PAID TO RENT THE VIDEOS INITIALLY
      *
      */

      DECLARE
         @v_rentfees DECIMAL(5, 2)

      /*
      *   SSMA informational messages:
      *   M2SS0134: Conversion of following Comment(s) is not supported : LATE FEES FOR PRIOR RENTALS
      *
      */

      DECLARE
         @v_overfees INT

      /*
      *   SSMA informational messages:
      *   M2SS0134: Conversion of following Comment(s) is not supported : SUM OF PAYMENTS MADE PREVIOUSLY
      *
      */

      DECLARE
         @v_payments DECIMAL(5, 2)

      SELECT @v_rentfees = ISNULL(SUM(film.rental_rate), 0)
      FROM film, inventory, rental
      WHERE 
         film.film_id = inventory.film_id AND 
         inventory.inventory_id = rental.inventory_id AND 
         rental.rental_date <= @p_effective_date AND 
         rental.customer_id = @p_customer_id

      SELECT @v_overfees = ISNULL(SUM(
         CASE 
            WHEN (((DATEDIFF(DAY, '1900-01-01', rental.return_date) + 693961) - (DATEDIFF(DAY, '1900-01-01', rental.rental_date) + 693961)) > film.rental_duration) THEN (((DATEDIFF(DAY, '1900-01-01', rental.return_date) + 693961) - (DATEDIFF(DAY, '1900-01-01', rental.rental_date) + 693961)) - film.rental_duration)
            ELSE 0
         END), 0)
      FROM rental, inventory, film
      WHERE 
         film.film_id = inventory.film_id AND 
         inventory.inventory_id = rental.inventory_id AND 
         rental.rental_date <= @p_effective_date AND 
         rental.customer_id = @p_customer_id

      SELECT @v_payments = ISNULL(SUM(payment.amount), 0)
      FROM payment
      WHERE payment.payment_date <= @p_effective_date AND payment.customer_id = @p_customer_id

      RETURN @v_rentfees + @v_overfees - @v_payments

   END

GO

/****** Object:  UserDefinedFunction [dbo].[inventory_held_by_customer]    Script Date: 10/27/2020 3:13:54 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[inventory_held_by_customer] 
( 
   @p_inventory_id INT
)
RETURNS INT
AS 
   BEGIN

      DECLARE
         @v_customer_id INT

      SELECT @v_customer_id = rental.customer_id
      FROM rental
      WHERE rental.return_date IS NULL AND rental.inventory_id = @p_inventory_id

      RETURN @v_customer_id

   END

GO


/****** Object:  UserDefinedFunction [dbo].[norm_enum$film$rating]    Script Date: 10/27/2020 3:14:42 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[norm_enum$film$rating] 
( 
   @setval NVARCHAR(MAX)
)
RETURNS NVARCHAR(MAX)
AS 
   BEGIN
      RETURN dbo.enum2str$film$rating(dbo.str2enum$film$rating(@setval))
   END

GO

/****** Object:  UserDefinedFunction [dbo].[norm_set$film$special_features]    Script Date: 10/27/2020 3:15:12 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[norm_set$film$special_features] 
( 
   @setval VARCHAR(MAX)
)
RETURNS VARCHAR(MAX)
AS 
   BEGIN
      RETURN dbo.set2str$film$special_features(dbo.str2set$film$special_features(@setval))
   END

GO


/****** Object:  UserDefinedFunction [dbo].[set2str$film$special_features]    Script Date: 10/27/2020 3:16:10 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[set2str$film$special_features] 
( 
   @setval BINARY(1)
)
RETURNS NVARCHAR(MAX)
AS 
   BEGIN

      IF (@setval IS NULL)
         RETURN NULL

      DECLARE
         @rv NVARCHAR(MAX)

      SET @rv = ''

      DECLARE
         @setval_bi BIGINT

      SET @setval_bi = @setval

      IF (@setval_bi & 0x1 = 0x1)
         SET @rv = @rv + ',' + 'Trailers'

      IF (@setval_bi & 0x2 = 0x2)
         SET @rv = @rv + ',' + 'Commentaries'

      IF (@setval_bi & 0x4 = 0x4)
         SET @rv = @rv + ',' + 'Deleted Scenes'

      IF (@setval_bi & 0x8 = 0x8)
         SET @rv = @rv + ',' + 'Behind the Scenes'

      IF (@rv = '')
         RETURN ''

      RETURN SUBSTRING(@rv, 2, LEN(@rv) - 1)

   END

GO


/****** Object:  UserDefinedFunction [dbo].[str2enum$film$rating]    Script Date: 10/27/2020 3:16:49 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[str2enum$film$rating] 
( 
   @setval NVARCHAR(MAX)
)
RETURNS TINYINT
AS 
   BEGIN
      RETURN 
         CASE @setval
            WHEN 'G' THEN 1
            WHEN 'PG' THEN 2
            WHEN 'PG-13' THEN 3
            WHEN 'R' THEN 4
            WHEN 'NC-17' THEN 5
            ELSE 0
         END
   END

GO


/****** Object:  UserDefinedFunction [dbo].[str2set$film$special_features]    Script Date: 10/27/2020 3:17:06 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[str2set$film$special_features] 
( 
   @setval NVARCHAR(MAX)
)
RETURNS BINARY(1)
AS 
   BEGIN

      IF (@setval IS NULL)
         RETURN NULL

      SET @setval = ',' + @setval + ','

      RETURN 
         CASE 
            WHEN @setval LIKE '%,Trailers,%' THEN 0x1
            ELSE CAST(0 AS BIGINT)
         END | 
         CASE 
            WHEN @setval LIKE '%,Commentaries,%' THEN 0x2
            ELSE CAST(0 AS BIGINT)
         END | 
         CASE 
            WHEN @setval LIKE '%,Deleted Scenes,%' THEN 0x4
            ELSE CAST(0 AS BIGINT)
         END | 
         CASE 
            WHEN @setval LIKE '%,Behind the Scenes,%' THEN 0x8
            ELSE CAST(0 AS BIGINT)
         END

   END

GO



