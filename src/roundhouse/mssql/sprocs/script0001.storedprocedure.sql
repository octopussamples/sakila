/****** Object:  StoredProcedure [dbo].[film_in_stock]    Script Date: 10/27/2020 3:17:55 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[film_in_stock]  
   @p_film_id INT,
   @p_store_id INT,
   @p_film_count INT  OUTPUT
AS 
   BEGIN

      SET  XACT_ABORT  ON

      SET  NOCOUNT  ON

      SET @p_film_count = NULL

      SELECT inventory.inventory_id
      FROM inventory
      WHERE 
         inventory.film_id = @p_film_id AND 
         inventory.store_id = @p_store_id AND 
         dbo.inventory_in_stock(inventory.inventory_id) <> 0

      SELECT @p_film_count = COUNT_BIG(*)
      FROM inventory
      WHERE 
         inventory.film_id = @p_film_id AND 
         inventory.store_id = @p_store_id AND 
         dbo.inventory_in_stock(inventory.inventory_id) <> 0

   END

GO


/****** Object:  StoredProcedure [dbo].[film_not_in_stock]    Script Date: 10/27/2020 3:41:08 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[film_not_in_stock]  
   @p_film_id INT,
   @p_store_id INT,
   @p_film_count INT  OUTPUT
AS 
   BEGIN

      SET  XACT_ABORT  ON

      SET  NOCOUNT  ON

      SET @p_film_count = NULL

      SELECT inventory.inventory_id
      FROM inventory
      WHERE 
         inventory.film_id = @p_film_id AND 
         inventory.store_id = @p_store_id AND 
         NOT dbo.inventory_in_stock(inventory.inventory_id) <> 0

      SELECT @p_film_count = COUNT_BIG(*)
      FROM inventory
      WHERE 
         inventory.film_id = @p_film_id AND 
         inventory.store_id = @p_store_id AND 
         NOT dbo.inventory_in_stock(inventory.inventory_id) <> 0

   END

GO


/****** Object:  StoredProcedure [dbo].[rewards_report]    Script Date: 10/27/2020 3:41:30 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[rewards_report]  
   @min_monthly_purchases TINYINT,
   @min_dollar_amount_purchased DECIMAL(10, 2),
   @count_rewardees INT  OUTPUT
AS 
   /*
   *   SSMA informational messages:
   *   M2SS0003: The following SQL clause was ignored during conversion:
   *   proc : .
   */

   BEGIN

      SET  XACT_ABORT  ON

      SET  NOCOUNT  ON

      SET @count_rewardees = NULL

      DECLARE
         @last_month_start date

      DECLARE
         @last_month_end date

      /*
      *   SSMA informational messages:
      *   M2SS0134: Conversion of following Comment(s) is not supported :  Some sanity checks... 
      *
      */

      IF @min_monthly_purchases = 0
         BEGIN

            SELECT N'Minimum monthly purchases parameter must be > 0'

            GOTO proc$leave

         END

      IF @min_dollar_amount_purchased = 0.00
         BEGIN

            SELECT N'Minimum monthly dollar amount purchased parameter must be > $0.00'

            GOTO proc$leave

         END

      SET @last_month_start = dateadd(month, -1, CAST(getdate() AS DATE))

      /* 
      *   SSMA error messages:
      *   M2SS0201: MySQL standard function STR_TO_DATE is not supported in current SSMA version

      SET @last_month_start = STR_TO_DATE((CAST(year(@last_month_start) AS varchar(50))) + (N'-') + (CAST(datepart(MONTH, @last_month_start) AS varchar(50))) + (N'-01'), '%Y-%m-%d')
      */



      SET @last_month_end = DATEADD(DAY, -1, DATEADD(MONTH, DATEDIFF(MONTH, CAST('1900-01-01' AS DATE), @last_month_start) + 1, CAST('1900-01-01' AS DATE)))

      CREATE TABLE #tmpCustomer
      (
         customer_id INT NOT NULL PRIMARY KEY
      )

      INSERT #tmpCustomer(customer_id)
         SELECT p.customer_id
         FROM payment  AS p
         WHERE p.payment_date BETWEEN @last_month_start AND @last_month_end
         GROUP BY p.customer_id
         HAVING SUM(p.amount) > @min_dollar_amount_purchased AND COUNT_BIG(p.customer_id) > @min_monthly_purchases
            ORDER BY p.customer_id

      /*
      *   SSMA informational messages:
      *   M2SS0134: Conversion of following Comment(s) is not supported :  Populate OUT parameter with count of found customers 
      *
      */

      SELECT @count_rewardees = COUNT_BIG(*)
      FROM #tmpCustomer

      /*
      *   SSMA informational messages:
      *   M2SS0134: Conversion of following Comment(s) is not supported : 
      *                       Output ALL customer information of matching rewardees.
      *                       Customize output as needed.
      *                   
      *
      */

      SELECT 
         c.customer_id, 
         c.store_id, 
         c.first_name, 
         c.last_name, 
         c.email, 
         c.address_id, 
         c.active, 
         c.create_date, 
         c.last_update
      FROM 
         #tmpCustomer  AS t 
            INNER JOIN customer  AS c 
            ON t.customer_id = c.customer_id

      DROP TABLE #tmpCustomer

   END
   proc$leave:

GO


