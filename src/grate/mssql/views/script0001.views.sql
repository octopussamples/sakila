/****** Object:  View [dbo].[actor_info]    Script Date: 10/27/2020 4:38:34 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[actor_info] (
   [actor_id], 
   [first_name], 
   [last_name], 
   [film_info])
AS 
   SELECT a.actor_id AS actor_id, a.first_name AS first_name, a.last_name AS last_name, NULL AS film_info
   FROM (((actor  AS a 
      LEFT JOIN film_actor  AS fa 
      ON ((a.actor_id = fa.actor_id))) 
      LEFT JOIN film_category  AS fc 
      ON ((fa.film_id = fc.film_id))) 
      LEFT JOIN category  AS c 
      ON ((fc.category_id = c.category_id)))
   GROUP BY a.actor_id, a.first_name, a.last_name





GO


/****** Object:  View [dbo].[customer_list]    Script Date: 10/27/2020 4:38:52 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[customer_list] (
   [ID], 
   [name], 
   [address], 
   [zip code], 
   [phone], 
   [city], 
   [country], 
   [notes], 
   [SID])
AS 
   SELECT 
      cu.customer_id AS ID, 
      cu.first_name + N' ' + cu.last_name AS name, 
      a.address AS address, 
      a.postal_code AS [zip code], 
      a.phone AS phone, 
      city.city AS city, 
      country.country AS country, 
      CASE 
         WHEN (CAST(cu.active AS bigint) <> 0) THEN N'active'
         ELSE N''
      END AS notes, 
      cu.store_id AS SID
   FROM (((customer  AS cu 
      INNER JOIN address  AS a 
      ON ((cu.address_id = a.address_id))) 
      INNER JOIN city 
      ON ((a.city_id = city.city_id))) 
      INNER JOIN country 
      ON ((city.country_id = country.country_id)))

GO


/****** Object:  View [dbo].[film_list]    Script Date: 10/27/2020 4:39:22 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[film_list] (
   [FID], 
   [title], 
   [description], 
   [category], 
   [price], 
   [length], 
   [rating], 
   [actors])
AS 
   SELECT 
      film.film_id AS FID, 
      film.title AS title, 
      film.description AS description, 
      category.name AS category, 
      film.rental_rate AS price, 
      film.length AS length, 
      film.rating AS rating, 
      NULL AS actors
   FROM ((((category 
      LEFT JOIN film_category 
      ON ((category.category_id = film_category.category_id))) 
      LEFT JOIN film 
      ON ((film_category.film_id = film.film_id))) 
      INNER JOIN film_actor 
      ON ((film.film_id = film_actor.film_id))) 
      INNER JOIN actor 
      ON ((film_actor.actor_id = actor.actor_id)))
   GROUP BY film.film_id, film.title, film.description, category.name, film.rental_rate, film.length, film.rating






GO


/****** Object:  View [dbo].[nicer_but_slower_film_list]    Script Date: 10/27/2020 4:39:48 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[nicer_but_slower_film_list] (
   [FID], 
   [title], 
   [description], 
   [category], 
   [price], 
   [length], 
   [rating], 
   [actors])
AS 
   SELECT 
      film.film_id AS FID, 
      film.title AS title, 
      film.description AS description, 
      category.name AS category, 
      film.rental_rate AS price, 
      film.length AS length, 
      film.rating AS rating, 
      NULL AS actors
   FROM ((((category 
      LEFT JOIN film_category 
      ON ((category.category_id = film_category.category_id))) 
      LEFT JOIN film 
      ON ((film_category.film_id = film.film_id))) 
      INNER JOIN film_actor 
      ON ((film.film_id = film_actor.film_id))) 
      INNER JOIN actor 
      ON ((film_actor.actor_id = actor.actor_id)))
   GROUP BY film.film_id, film.title, film.description, category.name, film.rental_rate, film.length, film.rating






GO


/****** Object:  View [dbo].[sales_by_film_category]    Script Date: 10/27/2020 4:40:14 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[sales_by_film_category] ([category], [total_sales])
AS 
   SELECT TOP (9223372036854775807) c.name AS category, sum(p.amount) AS total_sales
   FROM (((((payment  AS p 
      INNER JOIN rental  AS r 
      ON ((p.rental_id = r.rental_id))) 
      INNER JOIN inventory  AS i 
      ON ((r.inventory_id = i.inventory_id))) 
      INNER JOIN film  AS f 
      ON ((i.film_id = f.film_id))) 
      INNER JOIN film_category  AS fc 
      ON ((f.film_id = fc.film_id))) 
      INNER JOIN category  AS c 
      ON ((fc.category_id = c.category_id)))
   GROUP BY c.name
      ORDER BY total_sales DESC

GO


/****** Object:  View [dbo].[sales_by_store]    Script Date: 10/27/2020 4:40:36 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[sales_by_store] ([store], [manager], [total_sales])
AS 
   /*
   *   SSMA warning messages:
   *   M2SS0104: Non aggregated column COUNTRY is aggregated with Min(..) in Select, Orderby and Having clauses.
   *   M2SS0104: Non aggregated column CITY is aggregated with Min(..) in Select, Orderby and Having clauses.
   *   M2SS0104: Non aggregated column CITY is aggregated with Min(..) in Select, Orderby and Having clauses.
   *   M2SS0104: Non aggregated column COUNTRY is aggregated with Min(..) in Select, Orderby and Having clauses.
   *   M2SS0104: Non aggregated column FIRST_NAME is aggregated with Min(..) in Select, Orderby and Having clauses.
   *   M2SS0104: Non aggregated column LAST_NAME is aggregated with Min(..) in Select, Orderby and Having clauses.
   */

   SELECT TOP (9223372036854775807) min(c.city) + N',' + min(cy.country) AS store, min(m.first_name) + N' ' + min(m.last_name) AS manager, sum(p.amount) AS total_sales
   FROM (((((((payment  AS p 
      INNER JOIN rental  AS r 
      ON ((p.rental_id = r.rental_id))) 
      INNER JOIN inventory  AS i 
      ON ((r.inventory_id = i.inventory_id))) 
      INNER JOIN store  AS s 
      ON ((i.store_id = s.store_id))) 
      INNER JOIN address  AS a 
      ON ((s.address_id = a.address_id))) 
      INNER JOIN city  AS c 
      ON ((a.city_id = c.city_id))) 
      INNER JOIN country  AS cy 
      ON ((c.country_id = cy.country_id))) 
      INNER JOIN staff  AS m 
      ON ((s.manager_staff_id = m.staff_id)))
   GROUP BY s.store_id
      ORDER BY min(cy.country), min(c.city)

GO


/****** Object:  View [dbo].[staff_list]    Script Date: 10/27/2020 4:40:57 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[staff_list] (
   [ID], 
   [name], 
   [address], 
   [zip code], 
   [phone], 
   [city], 
   [country], 
   [SID])
AS 
   SELECT 
      s.staff_id AS ID, 
      s.first_name + N' ' + s.last_name AS name, 
      a.address AS address, 
      a.postal_code AS [zip code], 
      a.phone AS phone, 
      city.city AS city, 
      country.country AS country, 
      s.store_id AS SID
   FROM (((staff  AS s 
      INNER JOIN address  AS a 
      ON ((s.address_id = a.address_id))) 
      INNER JOIN city 
      ON ((a.city_id = city.city_id))) 
      INNER JOIN country 
      ON ((city.country_id = country.country_id)))

GO


