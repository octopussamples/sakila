IF EXISTS (SELECT * FROM sys.objects so JOIN sys.schemas sc ON so.schema_id = sc.schema_id WHERE so.name = N'actor'  AND sc.name = N'dbo'  AND type in (N'U'))
BEGIN

  DECLARE @drop_statement nvarchar(500)

  DECLARE drop_cursor CURSOR FOR
      SELECT 'alter table '+quotename(schema_name(ob.schema_id))+
      '.'+quotename(object_name(ob.object_id))+ ' drop constraint ' + quotename(fk.name) 
      FROM sys.objects ob INNER JOIN sys.foreign_keys fk ON fk.parent_object_id = ob.object_id
      WHERE fk.referenced_object_id = 
          (
             SELECT so.object_id 
             FROM sys.objects so JOIN sys.schemas sc
             ON so.schema_id = sc.schema_id
             WHERE so.name = N'actor'  AND sc.name = N'dbo'  AND type in (N'U')
           )

  OPEN drop_cursor

  FETCH NEXT FROM drop_cursor
  INTO @drop_statement

  WHILE @@FETCH_STATUS = 0
  BEGIN
     EXEC (@drop_statement)

     FETCH NEXT FROM drop_cursor
     INTO @drop_statement
  END

  CLOSE drop_cursor
  DEALLOCATE drop_cursor

  DROP TABLE [actor]
END 
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE 
[actor]
(
   [actor_id] int IDENTITY(1, 1)  NOT NULL,
   [first_name] nvarchar(45)  NOT NULL,
   [last_name] nvarchar(45)  NOT NULL,
   [last_update] datetime  NOT NULL
)
WITH (DATA_COMPRESSION = NONE)
GO


GO
IF EXISTS (SELECT * FROM sys.objects so JOIN sys.schemas sc ON so.schema_id = sc.schema_id WHERE so.name = N'address'  AND sc.name = N'dbo'  AND type in (N'U'))
BEGIN

  DECLARE @drop_statement nvarchar(500)

  DECLARE drop_cursor CURSOR FOR
      SELECT 'alter table '+quotename(schema_name(ob.schema_id))+
      '.'+quotename(object_name(ob.object_id))+ ' drop constraint ' + quotename(fk.name) 
      FROM sys.objects ob INNER JOIN sys.foreign_keys fk ON fk.parent_object_id = ob.object_id
      WHERE fk.referenced_object_id = 
          (
             SELECT so.object_id 
             FROM sys.objects so JOIN sys.schemas sc
             ON so.schema_id = sc.schema_id
             WHERE so.name = N'address'  AND sc.name = N'dbo'  AND type in (N'U')
           )

  OPEN drop_cursor

  FETCH NEXT FROM drop_cursor
  INTO @drop_statement

  WHILE @@FETCH_STATUS = 0
  BEGIN
     EXEC (@drop_statement)

     FETCH NEXT FROM drop_cursor
     INTO @drop_statement
  END

  CLOSE drop_cursor
  DEALLOCATE drop_cursor

  DROP TABLE [address]
END 
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE 
[address]
(
   [address_id] int IDENTITY(1, 1)  NOT NULL,
   [address] nvarchar(50)  NOT NULL,
   [address2] nvarchar(50)  NULL,
   [district] nvarchar(20)  NOT NULL,
   [city_id] int  NOT NULL,
   [postal_code] nvarchar(10)  NULL,
   [phone] nvarchar(20)  NOT NULL,
   [location] geometry  NOT NULL,
   [last_update] datetime  NOT NULL
)
WITH (DATA_COMPRESSION = NONE)
GO


GO
IF EXISTS (SELECT * FROM sys.objects so JOIN sys.schemas sc ON so.schema_id = sc.schema_id WHERE so.name = N'category'  AND sc.name = N'dbo'  AND type in (N'U'))
BEGIN

  DECLARE @drop_statement nvarchar(500)

  DECLARE drop_cursor CURSOR FOR
      SELECT 'alter table '+quotename(schema_name(ob.schema_id))+
      '.'+quotename(object_name(ob.object_id))+ ' drop constraint ' + quotename(fk.name) 
      FROM sys.objects ob INNER JOIN sys.foreign_keys fk ON fk.parent_object_id = ob.object_id
      WHERE fk.referenced_object_id = 
          (
             SELECT so.object_id 
             FROM sys.objects so JOIN sys.schemas sc
             ON so.schema_id = sc.schema_id
             WHERE so.name = N'category'  AND sc.name = N'dbo'  AND type in (N'U')
           )

  OPEN drop_cursor

  FETCH NEXT FROM drop_cursor
  INTO @drop_statement

  WHILE @@FETCH_STATUS = 0
  BEGIN
     EXEC (@drop_statement)

     FETCH NEXT FROM drop_cursor
     INTO @drop_statement
  END

  CLOSE drop_cursor
  DEALLOCATE drop_cursor

  DROP TABLE [category]
END 
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE 
[category]
(
   [category_id] tinyint IDENTITY(1, 1)  NOT NULL,
   [name] nvarchar(25)  NOT NULL,
   [last_update] datetime  NOT NULL
)
WITH (DATA_COMPRESSION = NONE)
GO



GO
IF EXISTS (SELECT * FROM sys.objects so JOIN sys.schemas sc ON so.schema_id = sc.schema_id WHERE so.name = N'city'  AND sc.name = N'dbo'  AND type in (N'U'))
BEGIN

  DECLARE @drop_statement nvarchar(500)

  DECLARE drop_cursor CURSOR FOR
      SELECT 'alter table '+quotename(schema_name(ob.schema_id))+
      '.'+quotename(object_name(ob.object_id))+ ' drop constraint ' + quotename(fk.name) 
      FROM sys.objects ob INNER JOIN sys.foreign_keys fk ON fk.parent_object_id = ob.object_id
      WHERE fk.referenced_object_id = 
          (
             SELECT so.object_id 
             FROM sys.objects so JOIN sys.schemas sc
             ON so.schema_id = sc.schema_id
             WHERE so.name = N'city'  AND sc.name = N'dbo'  AND type in (N'U')
           )

  OPEN drop_cursor

  FETCH NEXT FROM drop_cursor
  INTO @drop_statement

  WHILE @@FETCH_STATUS = 0
  BEGIN
     EXEC (@drop_statement)

     FETCH NEXT FROM drop_cursor
     INTO @drop_statement
  END

  CLOSE drop_cursor
  DEALLOCATE drop_cursor

  DROP TABLE [city]
END 
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE 
[city]
(
   [city_id] int IDENTITY(1, 1)  NOT NULL,
   [city] nvarchar(50)  NOT NULL,
   [country_id] int  NOT NULL,
   [last_update] datetime  NOT NULL
)
WITH (DATA_COMPRESSION = NONE)
GO


GO
IF EXISTS (SELECT * FROM sys.objects so JOIN sys.schemas sc ON so.schema_id = sc.schema_id WHERE so.name = N'country'  AND sc.name = N'dbo'  AND type in (N'U'))
BEGIN

  DECLARE @drop_statement nvarchar(500)

  DECLARE drop_cursor CURSOR FOR
      SELECT 'alter table '+quotename(schema_name(ob.schema_id))+
      '.'+quotename(object_name(ob.object_id))+ ' drop constraint ' + quotename(fk.name) 
      FROM sys.objects ob INNER JOIN sys.foreign_keys fk ON fk.parent_object_id = ob.object_id
      WHERE fk.referenced_object_id = 
          (
             SELECT so.object_id 
             FROM sys.objects so JOIN sys.schemas sc
             ON so.schema_id = sc.schema_id
             WHERE so.name = N'country'  AND sc.name = N'dbo'  AND type in (N'U')
           )

  OPEN drop_cursor

  FETCH NEXT FROM drop_cursor
  INTO @drop_statement

  WHILE @@FETCH_STATUS = 0
  BEGIN
     EXEC (@drop_statement)

     FETCH NEXT FROM drop_cursor
     INTO @drop_statement
  END

  CLOSE drop_cursor
  DEALLOCATE drop_cursor

  DROP TABLE [country]
END 
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE 
[country]
(
   [country_id] int IDENTITY(1, 1)  NOT NULL,
   [country] nvarchar(50)  NOT NULL,
   [last_update] datetime  NOT NULL
)
WITH (DATA_COMPRESSION = NONE)
GO


GO
IF EXISTS (SELECT * FROM sys.objects so JOIN sys.schemas sc ON so.schema_id = sc.schema_id WHERE so.name = N'customer'  AND sc.name = N'dbo'  AND type in (N'U'))
BEGIN

  DECLARE @drop_statement nvarchar(500)

  DECLARE drop_cursor CURSOR FOR
      SELECT 'alter table '+quotename(schema_name(ob.schema_id))+
      '.'+quotename(object_name(ob.object_id))+ ' drop constraint ' + quotename(fk.name) 
      FROM sys.objects ob INNER JOIN sys.foreign_keys fk ON fk.parent_object_id = ob.object_id
      WHERE fk.referenced_object_id = 
          (
             SELECT so.object_id 
             FROM sys.objects so JOIN sys.schemas sc
             ON so.schema_id = sc.schema_id
             WHERE so.name = N'customer'  AND sc.name = N'dbo'  AND type in (N'U')
           )

  OPEN drop_cursor

  FETCH NEXT FROM drop_cursor
  INTO @drop_statement

  WHILE @@FETCH_STATUS = 0
  BEGIN
     EXEC (@drop_statement)

     FETCH NEXT FROM drop_cursor
     INTO @drop_statement
  END

  CLOSE drop_cursor
  DEALLOCATE drop_cursor

  DROP TABLE [customer]
END 
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE 
[customer]
(
   [customer_id] int IDENTITY(1, 1)  NOT NULL,
   [store_id] tinyint  NOT NULL,
   [first_name] nvarchar(45)  NOT NULL,
   [last_name] nvarchar(45)  NOT NULL,
   [email] nvarchar(50)  NULL,
   [address_id] int  NOT NULL,

   /*
   *   SSMA informational messages:
   *   M2SS0052: BIT literal was converted to BINARY literal
   */

   [active] binary(1)  NOT NULL,
   [create_date] datetime2(0)  NOT NULL,
   [last_update] datetime  NULL
)
WITH (DATA_COMPRESSION = NONE)
GO


GO
IF EXISTS (SELECT * FROM sys.objects so JOIN sys.schemas sc ON so.schema_id = sc.schema_id WHERE so.name = N'databasechangelog'  AND sc.name = N'dbo'  AND type in (N'U'))
BEGIN

  DECLARE @drop_statement nvarchar(500)

  DECLARE drop_cursor CURSOR FOR
      SELECT 'alter table '+quotename(schema_name(ob.schema_id))+
      '.'+quotename(object_name(ob.object_id))+ ' drop constraint ' + quotename(fk.name) 
      FROM sys.objects ob INNER JOIN sys.foreign_keys fk ON fk.parent_object_id = ob.object_id
      WHERE fk.referenced_object_id = 
          (
             SELECT so.object_id 
             FROM sys.objects so JOIN sys.schemas sc
             ON so.schema_id = sc.schema_id
             WHERE so.name = N'databasechangelog'  AND sc.name = N'dbo'  AND type in (N'U')
           )

  OPEN drop_cursor

  FETCH NEXT FROM drop_cursor
  INTO @drop_statement

  WHILE @@FETCH_STATUS = 0
  BEGIN
     EXEC (@drop_statement)

     FETCH NEXT FROM drop_cursor
     INTO @drop_statement
  END

  CLOSE drop_cursor
  DEALLOCATE drop_cursor

  DROP TABLE [databasechangelog]
END 
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE 
[databasechangelog]
(
   [ID] nvarchar(255)  NOT NULL,
   [AUTHOR] nvarchar(255)  NOT NULL,
   [FILENAME] nvarchar(255)  NOT NULL,
   [DATEEXECUTED] datetime2(0)  NOT NULL,
   [ORDEREXECUTED] int  NOT NULL,
   [EXECTYPE] nvarchar(10)  NOT NULL,
   [MD5SUM] nvarchar(35)  NULL,
   [DESCRIPTION] nvarchar(255)  NULL,
   [COMMENTS] nvarchar(255)  NULL,
   [TAG] nvarchar(255)  NULL,
   [LIQUIBASE] nvarchar(20)  NULL,
   [CONTEXTS] nvarchar(255)  NULL,
   [LABELS] nvarchar(255)  NULL,
   [DEPLOYMENT_ID] nvarchar(10)  NULL
)
WITH (DATA_COMPRESSION = NONE)
GO


GO
IF EXISTS (SELECT * FROM sys.objects so JOIN sys.schemas sc ON so.schema_id = sc.schema_id WHERE so.name = N'databasechangeloglock'  AND sc.name = N'dbo'  AND type in (N'U'))
BEGIN

  DECLARE @drop_statement nvarchar(500)

  DECLARE drop_cursor CURSOR FOR
      SELECT 'alter table '+quotename(schema_name(ob.schema_id))+
      '.'+quotename(object_name(ob.object_id))+ ' drop constraint ' + quotename(fk.name) 
      FROM sys.objects ob INNER JOIN sys.foreign_keys fk ON fk.parent_object_id = ob.object_id
      WHERE fk.referenced_object_id = 
          (
             SELECT so.object_id 
             FROM sys.objects so JOIN sys.schemas sc
             ON so.schema_id = sc.schema_id
             WHERE so.name = N'databasechangeloglock'  AND sc.name = N'dbo'  AND type in (N'U')
           )

  OPEN drop_cursor

  FETCH NEXT FROM drop_cursor
  INTO @drop_statement

  WHILE @@FETCH_STATUS = 0
  BEGIN
     EXEC (@drop_statement)

     FETCH NEXT FROM drop_cursor
     INTO @drop_statement
  END

  CLOSE drop_cursor
  DEALLOCATE drop_cursor

  DROP TABLE [databasechangeloglock]
END 
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE 
[databasechangeloglock]
(
   [ID] int  NOT NULL,
   [LOCKED] binary(1)  NOT NULL,
   [LOCKGRANTED] datetime2(0)  NULL,
   [LOCKEDBY] nvarchar(255)  NULL
)
WITH (DATA_COMPRESSION = NONE)
GO


GO
IF EXISTS (SELECT * FROM sys.objects so JOIN sys.schemas sc ON so.schema_id = sc.schema_id WHERE so.name = N'film'  AND sc.name = N'dbo'  AND type in (N'U'))
BEGIN

  DECLARE @drop_statement nvarchar(500)

  DECLARE drop_cursor CURSOR FOR
      SELECT 'alter table '+quotename(schema_name(ob.schema_id))+
      '.'+quotename(object_name(ob.object_id))+ ' drop constraint ' + quotename(fk.name) 
      FROM sys.objects ob INNER JOIN sys.foreign_keys fk ON fk.parent_object_id = ob.object_id
      WHERE fk.referenced_object_id = 
          (
             SELECT so.object_id 
             FROM sys.objects so JOIN sys.schemas sc
             ON so.schema_id = sc.schema_id
             WHERE so.name = N'film'  AND sc.name = N'dbo'  AND type in (N'U')
           )

  OPEN drop_cursor

  FETCH NEXT FROM drop_cursor
  INTO @drop_statement

  WHILE @@FETCH_STATUS = 0
  BEGIN
     EXEC (@drop_statement)

     FETCH NEXT FROM drop_cursor
     INTO @drop_statement
  END

  CLOSE drop_cursor
  DEALLOCATE drop_cursor

  DROP TABLE [film]
END 
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE 
[film]
(
   [film_id] int IDENTITY(1, 1)  NOT NULL,
   [title] nvarchar(128)  NOT NULL,
   [description] nvarchar(max)  NULL,
   [release_year] smallint  NULL,
   [language_id] tinyint  NOT NULL,
   [original_language_id] tinyint  NULL,

   /*
   *   SSMA informational messages:
   *   M2SS0052: string literal was converted to NUMERIC literal
   */

   [rental_duration] tinyint  NOT NULL,

   /*
   *   SSMA informational messages:
   *   M2SS0052: string literal was converted to NUMERIC literal
   */

   [rental_rate] decimal(4, 2)  NOT NULL,
   [length] int  NULL,

   /*
   *   SSMA informational messages:
   *   M2SS0052: string literal was converted to NUMERIC literal
   */

   [replacement_cost] decimal(5, 2)  NOT NULL,
   [rating] nvarchar(5)  NULL,
   [special_features] nvarchar(54)  NULL,
   [last_update] datetime  NOT NULL
)
WITH (DATA_COMPRESSION = NONE)
GO


GO
IF EXISTS (SELECT * FROM sys.objects so JOIN sys.schemas sc ON so.schema_id = sc.schema_id WHERE so.name = N'film_actor'  AND sc.name = N'dbo'  AND type in (N'U'))
BEGIN

  DECLARE @drop_statement nvarchar(500)

  DECLARE drop_cursor CURSOR FOR
      SELECT 'alter table '+quotename(schema_name(ob.schema_id))+
      '.'+quotename(object_name(ob.object_id))+ ' drop constraint ' + quotename(fk.name) 
      FROM sys.objects ob INNER JOIN sys.foreign_keys fk ON fk.parent_object_id = ob.object_id
      WHERE fk.referenced_object_id = 
          (
             SELECT so.object_id 
             FROM sys.objects so JOIN sys.schemas sc
             ON so.schema_id = sc.schema_id
             WHERE so.name = N'film_actor'  AND sc.name = N'dbo'  AND type in (N'U')
           )

  OPEN drop_cursor

  FETCH NEXT FROM drop_cursor
  INTO @drop_statement

  WHILE @@FETCH_STATUS = 0
  BEGIN
     EXEC (@drop_statement)

     FETCH NEXT FROM drop_cursor
     INTO @drop_statement
  END

  CLOSE drop_cursor
  DEALLOCATE drop_cursor

  DROP TABLE [film_actor]
END 
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE 
[film_actor]
(
   [actor_id] int  NOT NULL,
   [film_id] int  NOT NULL,
   [last_update] datetime  NOT NULL
)
WITH (DATA_COMPRESSION = NONE)
GO


GO
IF EXISTS (SELECT * FROM sys.objects so JOIN sys.schemas sc ON so.schema_id = sc.schema_id WHERE so.name = N'film_category'  AND sc.name = N'dbo'  AND type in (N'U'))
BEGIN

  DECLARE @drop_statement nvarchar(500)

  DECLARE drop_cursor CURSOR FOR
      SELECT 'alter table '+quotename(schema_name(ob.schema_id))+
      '.'+quotename(object_name(ob.object_id))+ ' drop constraint ' + quotename(fk.name) 
      FROM sys.objects ob INNER JOIN sys.foreign_keys fk ON fk.parent_object_id = ob.object_id
      WHERE fk.referenced_object_id = 
          (
             SELECT so.object_id 
             FROM sys.objects so JOIN sys.schemas sc
             ON so.schema_id = sc.schema_id
             WHERE so.name = N'film_category'  AND sc.name = N'dbo'  AND type in (N'U')
           )

  OPEN drop_cursor

  FETCH NEXT FROM drop_cursor
  INTO @drop_statement

  WHILE @@FETCH_STATUS = 0
  BEGIN
     EXEC (@drop_statement)

     FETCH NEXT FROM drop_cursor
     INTO @drop_statement
  END

  CLOSE drop_cursor
  DEALLOCATE drop_cursor

  DROP TABLE [film_category]
END 
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE 
[film_category]
(
   [film_id] int  NOT NULL,
   [category_id] tinyint  NOT NULL,
   [last_update] datetime  NOT NULL
)
WITH (DATA_COMPRESSION = NONE)
GO


GO
IF EXISTS (SELECT * FROM sys.objects so JOIN sys.schemas sc ON so.schema_id = sc.schema_id WHERE so.name = N'film_text'  AND sc.name = N'dbo'  AND type in (N'U'))
BEGIN

  DECLARE @drop_statement nvarchar(500)

  DECLARE drop_cursor CURSOR FOR
      SELECT 'alter table '+quotename(schema_name(ob.schema_id))+
      '.'+quotename(object_name(ob.object_id))+ ' drop constraint ' + quotename(fk.name) 
      FROM sys.objects ob INNER JOIN sys.foreign_keys fk ON fk.parent_object_id = ob.object_id
      WHERE fk.referenced_object_id = 
          (
             SELECT so.object_id 
             FROM sys.objects so JOIN sys.schemas sc
             ON so.schema_id = sc.schema_id
             WHERE so.name = N'film_text'  AND sc.name = N'dbo'  AND type in (N'U')
           )

  OPEN drop_cursor

  FETCH NEXT FROM drop_cursor
  INTO @drop_statement

  WHILE @@FETCH_STATUS = 0
  BEGIN
     EXEC (@drop_statement)

     FETCH NEXT FROM drop_cursor
     INTO @drop_statement
  END

  CLOSE drop_cursor
  DEALLOCATE drop_cursor

  DROP TABLE [film_text]
END 
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE 
[film_text]
(
   [film_id] smallint  NOT NULL,
   [title] nvarchar(255)  NOT NULL,
   [description] nvarchar(max)  NULL
)
WITH (DATA_COMPRESSION = NONE)
GO


GO
IF EXISTS (SELECT * FROM sys.objects so JOIN sys.schemas sc ON so.schema_id = sc.schema_id WHERE so.name = N'inventory'  AND sc.name = N'dbo'  AND type in (N'U'))
BEGIN

  DECLARE @drop_statement nvarchar(500)

  DECLARE drop_cursor CURSOR FOR
      SELECT 'alter table '+quotename(schema_name(ob.schema_id))+
      '.'+quotename(object_name(ob.object_id))+ ' drop constraint ' + quotename(fk.name) 
      FROM sys.objects ob INNER JOIN sys.foreign_keys fk ON fk.parent_object_id = ob.object_id
      WHERE fk.referenced_object_id = 
          (
             SELECT so.object_id 
             FROM sys.objects so JOIN sys.schemas sc
             ON so.schema_id = sc.schema_id
             WHERE so.name = N'inventory'  AND sc.name = N'dbo'  AND type in (N'U')
           )

  OPEN drop_cursor

  FETCH NEXT FROM drop_cursor
  INTO @drop_statement

  WHILE @@FETCH_STATUS = 0
  BEGIN
     EXEC (@drop_statement)

     FETCH NEXT FROM drop_cursor
     INTO @drop_statement
  END

  CLOSE drop_cursor
  DEALLOCATE drop_cursor

  DROP TABLE [inventory]
END 
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE 
[inventory]
(
   [inventory_id] int IDENTITY(1, 1)  NOT NULL,
   [film_id] int  NOT NULL,
   [store_id] tinyint  NOT NULL,
   [last_update] datetime  NOT NULL
)
WITH (DATA_COMPRESSION = NONE)
GO


GO
IF EXISTS (SELECT * FROM sys.objects so JOIN sys.schemas sc ON so.schema_id = sc.schema_id WHERE so.name = N'language'  AND sc.name = N'dbo'  AND type in (N'U'))
BEGIN

  DECLARE @drop_statement nvarchar(500)

  DECLARE drop_cursor CURSOR FOR
      SELECT 'alter table '+quotename(schema_name(ob.schema_id))+
      '.'+quotename(object_name(ob.object_id))+ ' drop constraint ' + quotename(fk.name) 
      FROM sys.objects ob INNER JOIN sys.foreign_keys fk ON fk.parent_object_id = ob.object_id
      WHERE fk.referenced_object_id = 
          (
             SELECT so.object_id 
             FROM sys.objects so JOIN sys.schemas sc
             ON so.schema_id = sc.schema_id
             WHERE so.name = N'language'  AND sc.name = N'dbo'  AND type in (N'U')
           )

  OPEN drop_cursor

  FETCH NEXT FROM drop_cursor
  INTO @drop_statement

  WHILE @@FETCH_STATUS = 0
  BEGIN
     EXEC (@drop_statement)

     FETCH NEXT FROM drop_cursor
     INTO @drop_statement
  END

  CLOSE drop_cursor
  DEALLOCATE drop_cursor

  DROP TABLE [language]
END 
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE 
[language]
(
   [language_id] tinyint IDENTITY(1, 1)  NOT NULL,
   [name] nchar(20)  NOT NULL,
   [last_update] datetime  NOT NULL
)
WITH (DATA_COMPRESSION = NONE)
GO


GO
IF EXISTS (SELECT * FROM sys.objects so JOIN sys.schemas sc ON so.schema_id = sc.schema_id WHERE so.name = N'payment'  AND sc.name = N'dbo'  AND type in (N'U'))
BEGIN

  DECLARE @drop_statement nvarchar(500)

  DECLARE drop_cursor CURSOR FOR
      SELECT 'alter table '+quotename(schema_name(ob.schema_id))+
      '.'+quotename(object_name(ob.object_id))+ ' drop constraint ' + quotename(fk.name) 
      FROM sys.objects ob INNER JOIN sys.foreign_keys fk ON fk.parent_object_id = ob.object_id
      WHERE fk.referenced_object_id = 
          (
             SELECT so.object_id 
             FROM sys.objects so JOIN sys.schemas sc
             ON so.schema_id = sc.schema_id
             WHERE so.name = N'payment'  AND sc.name = N'dbo'  AND type in (N'U')
           )

  OPEN drop_cursor

  FETCH NEXT FROM drop_cursor
  INTO @drop_statement

  WHILE @@FETCH_STATUS = 0
  BEGIN
     EXEC (@drop_statement)

     FETCH NEXT FROM drop_cursor
     INTO @drop_statement
  END

  CLOSE drop_cursor
  DEALLOCATE drop_cursor

  DROP TABLE [payment]
END 
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE 
[payment]
(
   [payment_id] int IDENTITY(1, 1)  NOT NULL,
   [customer_id] int  NOT NULL,
   [staff_id] tinyint  NOT NULL,
   [rental_id] int  NULL,
   [amount] decimal(5, 2)  NOT NULL,
   [payment_date] datetime2(0)  NOT NULL,
   [last_update] datetime  NULL
)
WITH (DATA_COMPRESSION = NONE)
GO


GO
IF EXISTS (SELECT * FROM sys.objects so JOIN sys.schemas sc ON so.schema_id = sc.schema_id WHERE so.name = N'rental'  AND sc.name = N'dbo'  AND type in (N'U'))
BEGIN

  DECLARE @drop_statement nvarchar(500)

  DECLARE drop_cursor CURSOR FOR
      SELECT 'alter table '+quotename(schema_name(ob.schema_id))+
      '.'+quotename(object_name(ob.object_id))+ ' drop constraint ' + quotename(fk.name) 
      FROM sys.objects ob INNER JOIN sys.foreign_keys fk ON fk.parent_object_id = ob.object_id
      WHERE fk.referenced_object_id = 
          (
             SELECT so.object_id 
             FROM sys.objects so JOIN sys.schemas sc
             ON so.schema_id = sc.schema_id
             WHERE so.name = N'rental'  AND sc.name = N'dbo'  AND type in (N'U')
           )

  OPEN drop_cursor

  FETCH NEXT FROM drop_cursor
  INTO @drop_statement

  WHILE @@FETCH_STATUS = 0
  BEGIN
     EXEC (@drop_statement)

     FETCH NEXT FROM drop_cursor
     INTO @drop_statement
  END

  CLOSE drop_cursor
  DEALLOCATE drop_cursor

  DROP TABLE [rental]
END 
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE 
[rental]
(
   [rental_id] int IDENTITY(1, 1)  NOT NULL,
   [rental_date] datetime2(0)  NOT NULL,
   [inventory_id] int  NOT NULL,
   [customer_id] int  NOT NULL,
   [return_date] datetime2(0)  NULL,
   [staff_id] tinyint  NOT NULL,
   [last_update] datetime  NOT NULL
)
WITH (DATA_COMPRESSION = NONE)
GO


GO
IF EXISTS (SELECT * FROM sys.objects so JOIN sys.schemas sc ON so.schema_id = sc.schema_id WHERE so.name = N'staff'  AND sc.name = N'dbo'  AND type in (N'U'))
BEGIN

  DECLARE @drop_statement nvarchar(500)

  DECLARE drop_cursor CURSOR FOR
      SELECT 'alter table '+quotename(schema_name(ob.schema_id))+
      '.'+quotename(object_name(ob.object_id))+ ' drop constraint ' + quotename(fk.name) 
      FROM sys.objects ob INNER JOIN sys.foreign_keys fk ON fk.parent_object_id = ob.object_id
      WHERE fk.referenced_object_id = 
          (
             SELECT so.object_id 
             FROM sys.objects so JOIN sys.schemas sc
             ON so.schema_id = sc.schema_id
             WHERE so.name = N'staff'  AND sc.name = N'dbo'  AND type in (N'U')
           )

  OPEN drop_cursor

  FETCH NEXT FROM drop_cursor
  INTO @drop_statement

  WHILE @@FETCH_STATUS = 0
  BEGIN
     EXEC (@drop_statement)

     FETCH NEXT FROM drop_cursor
     INTO @drop_statement
  END

  CLOSE drop_cursor
  DEALLOCATE drop_cursor

  DROP TABLE [staff]
END 
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE 
[staff]
(
   [staff_id] tinyint IDENTITY(1, 1)  NOT NULL,
   [first_name] nvarchar(45)  NOT NULL,
   [last_name] nvarchar(45)  NOT NULL,
   [address_id] int  NOT NULL,
   [picture] varbinary(max)  NULL,
   [email] nvarchar(50)  NULL,
   [store_id] tinyint  NOT NULL,

   /*
   *   SSMA informational messages:
   *   M2SS0052: BIT literal was converted to BINARY literal
   */

   [active] binary(1)  NOT NULL,
   [username] nvarchar(16)  NOT NULL,
   [password] nvarchar(40)  NULL,
   [last_update] datetime  NOT NULL
)
WITH (DATA_COMPRESSION = NONE)
GO



GO
IF EXISTS (SELECT * FROM sys.objects so JOIN sys.schemas sc ON so.schema_id = sc.schema_id WHERE so.name = N'store'  AND sc.name = N'dbo'  AND type in (N'U'))
BEGIN

  DECLARE @drop_statement nvarchar(500)

  DECLARE drop_cursor CURSOR FOR
      SELECT 'alter table '+quotename(schema_name(ob.schema_id))+
      '.'+quotename(object_name(ob.object_id))+ ' drop constraint ' + quotename(fk.name) 
      FROM sys.objects ob INNER JOIN sys.foreign_keys fk ON fk.parent_object_id = ob.object_id
      WHERE fk.referenced_object_id = 
          (
             SELECT so.object_id 
             FROM sys.objects so JOIN sys.schemas sc
             ON so.schema_id = sc.schema_id
             WHERE so.name = N'store'  AND sc.name = N'dbo'  AND type in (N'U')
           )

  OPEN drop_cursor

  FETCH NEXT FROM drop_cursor
  INTO @drop_statement

  WHILE @@FETCH_STATUS = 0
  BEGIN
     EXEC (@drop_statement)

     FETCH NEXT FROM drop_cursor
     INTO @drop_statement
  END

  CLOSE drop_cursor
  DEALLOCATE drop_cursor

  DROP TABLE [store]
END 
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE 
[store]
(
   [store_id] tinyint IDENTITY(1, 1)  NOT NULL,
   [manager_staff_id] tinyint  NOT NULL,
   [address_id] int  NOT NULL,
   [last_update] datetime  NOT NULL
)
WITH (DATA_COMPRESSION = NONE)
GO



GO
IF EXISTS (SELECT * FROM sys.objects so JOIN sys.schemas sc ON so.schema_id = sc.schema_id WHERE so.name = N'PK_actor_actor_id'  AND sc.name = N'dbo'  AND type in (N'PK'))
ALTER TABLE [actor] DROP CONSTRAINT [PK_actor_actor_id]
 GO



ALTER TABLE [actor]
 ADD CONSTRAINT [PK_actor_actor_id]
   PRIMARY KEY
   CLUSTERED ([actor_id] ASC)

GO



GO
IF EXISTS (SELECT * FROM sys.objects so JOIN sys.schemas sc ON so.schema_id = sc.schema_id WHERE so.name = N'PK_address_address_id'  AND sc.name = N'dbo'  AND type in (N'PK'))
ALTER TABLE [address] DROP CONSTRAINT [PK_address_address_id]
 GO



ALTER TABLE [address]
 ADD CONSTRAINT [PK_address_address_id]
   PRIMARY KEY
   CLUSTERED ([address_id] ASC)

GO



GO
IF EXISTS (SELECT * FROM sys.objects so JOIN sys.schemas sc ON so.schema_id = sc.schema_id WHERE so.name = N'PK_category_category_id'  AND sc.name = N'dbo'  AND type in (N'PK'))
ALTER TABLE [category] DROP CONSTRAINT [PK_category_category_id]
 GO



ALTER TABLE [category]
 ADD CONSTRAINT [PK_category_category_id]
   PRIMARY KEY
   CLUSTERED ([category_id] ASC)

GO



GO
IF EXISTS (SELECT * FROM sys.objects so JOIN sys.schemas sc ON so.schema_id = sc.schema_id WHERE so.name = N'PK_city_city_id'  AND sc.name = N'dbo'  AND type in (N'PK'))
ALTER TABLE [city] DROP CONSTRAINT [PK_city_city_id]
 GO



ALTER TABLE [city]
 ADD CONSTRAINT [PK_city_city_id]
   PRIMARY KEY
   CLUSTERED ([city_id] ASC)

GO



GO
IF EXISTS (SELECT * FROM sys.objects so JOIN sys.schemas sc ON so.schema_id = sc.schema_id WHERE so.name = N'PK_country_country_id'  AND sc.name = N'dbo'  AND type in (N'PK'))
ALTER TABLE [country] DROP CONSTRAINT [PK_country_country_id]
 GO



ALTER TABLE [country]
 ADD CONSTRAINT [PK_country_country_id]
   PRIMARY KEY
   CLUSTERED ([country_id] ASC)

GO



GO
IF EXISTS (SELECT * FROM sys.objects so JOIN sys.schemas sc ON so.schema_id = sc.schema_id WHERE so.name = N'PK_customer_customer_id'  AND sc.name = N'dbo'  AND type in (N'PK'))
ALTER TABLE [customer] DROP CONSTRAINT [PK_customer_customer_id]
 GO



ALTER TABLE [customer]
 ADD CONSTRAINT [PK_customer_customer_id]
   PRIMARY KEY
   CLUSTERED ([customer_id] ASC)

GO



GO
IF EXISTS (SELECT * FROM sys.objects so JOIN sys.schemas sc ON so.schema_id = sc.schema_id WHERE so.name = N'PK_databasechangeloglock_ID'  AND sc.name = N'dbo'  AND type in (N'PK'))
ALTER TABLE [databasechangeloglock] DROP CONSTRAINT [PK_databasechangeloglock_ID]
 GO



ALTER TABLE [databasechangeloglock]
 ADD CONSTRAINT [PK_databasechangeloglock_ID]
   PRIMARY KEY
   CLUSTERED ([ID] ASC)

GO



GO
IF EXISTS (SELECT * FROM sys.objects so JOIN sys.schemas sc ON so.schema_id = sc.schema_id WHERE so.name = N'PK_film_film_id'  AND sc.name = N'dbo'  AND type in (N'PK'))
ALTER TABLE [film] DROP CONSTRAINT [PK_film_film_id]
 GO



ALTER TABLE [film]
 ADD CONSTRAINT [PK_film_film_id]
   PRIMARY KEY
   CLUSTERED ([film_id] ASC)

GO



GO
IF EXISTS (SELECT * FROM sys.objects so JOIN sys.schemas sc ON so.schema_id = sc.schema_id WHERE so.name = N'PK_film_actor_actor_id'  AND sc.name = N'dbo'  AND type in (N'PK'))
ALTER TABLE [film_actor] DROP CONSTRAINT [PK_film_actor_actor_id]
 GO



ALTER TABLE [film_actor]
 ADD CONSTRAINT [PK_film_actor_actor_id]
   PRIMARY KEY
   CLUSTERED ([actor_id] ASC, [film_id] ASC)

GO



GO
IF EXISTS (SELECT * FROM sys.objects so JOIN sys.schemas sc ON so.schema_id = sc.schema_id WHERE so.name = N'PK_film_category_film_id'  AND sc.name = N'dbo'  AND type in (N'PK'))
ALTER TABLE [film_category] DROP CONSTRAINT [PK_film_category_film_id]
 GO



ALTER TABLE [film_category]
 ADD CONSTRAINT [PK_film_category_film_id]
   PRIMARY KEY
   CLUSTERED ([film_id] ASC, [category_id] ASC)

GO



GO
IF EXISTS (SELECT * FROM sys.objects so JOIN sys.schemas sc ON so.schema_id = sc.schema_id WHERE so.name = N'PK_film_text_film_id'  AND sc.name = N'dbo'  AND type in (N'PK'))
ALTER TABLE [film_text] DROP CONSTRAINT [PK_film_text_film_id]
 GO



ALTER TABLE [film_text]
 ADD CONSTRAINT [PK_film_text_film_id]
   PRIMARY KEY
   CLUSTERED ([film_id] ASC)

GO



GO
IF EXISTS (SELECT * FROM sys.objects so JOIN sys.schemas sc ON so.schema_id = sc.schema_id WHERE so.name = N'PK_inventory_inventory_id'  AND sc.name = N'dbo'  AND type in (N'PK'))
ALTER TABLE [inventory] DROP CONSTRAINT [PK_inventory_inventory_id]
 GO



ALTER TABLE [inventory]
 ADD CONSTRAINT [PK_inventory_inventory_id]
   PRIMARY KEY
   CLUSTERED ([inventory_id] ASC)

GO



GO
IF EXISTS (SELECT * FROM sys.objects so JOIN sys.schemas sc ON so.schema_id = sc.schema_id WHERE so.name = N'PK_language_language_id'  AND sc.name = N'dbo'  AND type in (N'PK'))
ALTER TABLE [language] DROP CONSTRAINT [PK_language_language_id]
 GO



ALTER TABLE [language]
 ADD CONSTRAINT [PK_language_language_id]
   PRIMARY KEY
   CLUSTERED ([language_id] ASC)

GO



GO
IF EXISTS (SELECT * FROM sys.objects so JOIN sys.schemas sc ON so.schema_id = sc.schema_id WHERE so.name = N'PK_payment_payment_id'  AND sc.name = N'dbo'  AND type in (N'PK'))
ALTER TABLE [payment] DROP CONSTRAINT [PK_payment_payment_id]
 GO



ALTER TABLE [payment]
 ADD CONSTRAINT [PK_payment_payment_id]
   PRIMARY KEY
   CLUSTERED ([payment_id] ASC)

GO



GO
IF EXISTS (SELECT * FROM sys.objects so JOIN sys.schemas sc ON so.schema_id = sc.schema_id WHERE so.name = N'PK_rental_rental_id'  AND sc.name = N'dbo'  AND type in (N'PK'))
ALTER TABLE [rental] DROP CONSTRAINT [PK_rental_rental_id]
 GO



ALTER TABLE [rental]
 ADD CONSTRAINT [PK_rental_rental_id]
   PRIMARY KEY
   CLUSTERED ([rental_id] ASC)

GO





GO
IF EXISTS (SELECT * FROM sys.objects so JOIN sys.schemas sc ON so.schema_id = sc.schema_id WHERE so.name = N'PK_staff_staff_id'  AND sc.name = N'dbo'  AND type in (N'PK'))
ALTER TABLE [staff] DROP CONSTRAINT [PK_staff_staff_id]
 GO



ALTER TABLE [staff]
 ADD CONSTRAINT [PK_staff_staff_id]
   PRIMARY KEY
   CLUSTERED ([staff_id] ASC)

GO



GO
IF EXISTS (SELECT * FROM sys.objects so JOIN sys.schemas sc ON so.schema_id = sc.schema_id WHERE so.name = N'PK_store_store_id'  AND sc.name = N'dbo'  AND type in (N'PK'))
ALTER TABLE [store] DROP CONSTRAINT [PK_store_store_id]
 GO



ALTER TABLE [store]
 ADD CONSTRAINT [PK_store_store_id]
   PRIMARY KEY
   CLUSTERED ([store_id] ASC)

GO



GO
IF EXISTS (SELECT * FROM sys.objects so JOIN sys.schemas sc ON so.schema_id = sc.schema_id WHERE so.name = N'rental$rental_date'  AND sc.name = N'dbo'  AND type in (N'UQ'))
ALTER TABLE [rental] DROP CONSTRAINT [rental$rental_date]
 GO



ALTER TABLE [rental]
 ADD CONSTRAINT [rental$rental_date]
 UNIQUE 
   NONCLUSTERED ([rental_date] ASC, [inventory_id] ASC, [customer_id] ASC)

GO



GO
IF EXISTS (SELECT * FROM sys.objects so JOIN sys.schemas sc ON so.schema_id = sc.schema_id WHERE so.name = N'store$manager_staff_id'  AND sc.name = N'dbo'  AND type in (N'UQ'))
ALTER TABLE [store] DROP CONSTRAINT [store$manager_staff_id]
 GO



ALTER TABLE [store]
 ADD CONSTRAINT [store$manager_staff_id]
 UNIQUE 
   NONCLUSTERED ([manager_staff_id] ASC)

GO
